﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="15008000">
	<Property Name="SMProvider.SMVersion" Type="Int">201310</Property>
	<Property Name="varPersistentID:{009F2AD3-094D-4BD2-BF6A-CBC4CF8A6C92}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK28-AC</Property>
	<Property Name="varPersistentID:{00FBF312-B6E0-4727-A99E-FB7703510B9F}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK01-HV</Property>
	<Property Name="varPersistentID:{028394A5-3618-4B55-9C44-A7FB067623B1}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK07-voltage</Property>
	<Property Name="varPersistentID:{03471F69-241E-40D6-9E1C-F199BE494764}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK10-HV</Property>
	<Property Name="varPersistentID:{04ECCF7E-90DB-46DD-995D-3F9056F47C45}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK15-HV</Property>
	<Property Name="varPersistentID:{0583FB0F-00C7-4D7D-BA2F-CD4D896658CB}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK12-voltage</Property>
	<Property Name="varPersistentID:{0592397E-A5A7-4CC6-846A-9676BC2A89A0}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK17-Status</Property>
	<Property Name="varPersistentID:{05BD4DAE-EFB4-4E62-A711-88F05B8135BB}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK12-AC</Property>
	<Property Name="varPersistentID:{06DC5677-7B1D-47D5-828D-005EE727A08F}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK04-Voltage-set</Property>
	<Property Name="varPersistentID:{0776AA2D-FD26-4FA7-B3A6-9D4870CBB83E}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK14-HV</Property>
	<Property Name="varPersistentID:{07F71709-0972-43DE-98D2-EA3E2D93F484}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK03-AC</Property>
	<Property Name="varPersistentID:{081A9E9A-9B98-45E6-8F81-016AB2F8DC34}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK20-current</Property>
	<Property Name="varPersistentID:{0A355753-A2DF-46DD-A94B-F8945149044C}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK16-HV</Property>
	<Property Name="varPersistentID:{0AA95861-45EE-4C1B-A099-2A904DD20E1E}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK28-voltage</Property>
	<Property Name="varPersistentID:{0ABE90E0-B9BD-426E-8432-FA0DA4C6F9C6}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK08-HV</Property>
	<Property Name="varPersistentID:{0AF02DBE-6360-4D86-BF21-08BC680DDC67}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK04-current</Property>
	<Property Name="varPersistentID:{0E40CD40-1D77-4500-83DD-485D2131B1C4}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK05-current</Property>
	<Property Name="varPersistentID:{0F8007C9-1C11-4DCC-80E0-B568104B62A8}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK24-voltage</Property>
	<Property Name="varPersistentID:{116D6044-71E6-480F-86F7-B9D5256A2C2B}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK12-HV</Property>
	<Property Name="varPersistentID:{13D7A159-DDA7-4E90-841C-3E26483ACF7F}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK08-voltage</Property>
	<Property Name="varPersistentID:{1596375C-1CFF-4889-90AA-DBAA5ACD09EF}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK28-current</Property>
	<Property Name="varPersistentID:{184744E9-8E99-43B2-90A2-86F23533422C}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK25-HV</Property>
	<Property Name="varPersistentID:{18E080E3-FA9D-4FF8-AF3F-1EBCF0443A9C}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK19-HV</Property>
	<Property Name="varPersistentID:{1A50726E-9D59-4936-9D59-EA852CE25C10}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK19-current</Property>
	<Property Name="varPersistentID:{1CC4539F-33B0-40D2-8198-ADB305931A03}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK02-voltage</Property>
	<Property Name="varPersistentID:{1E89CB41-503E-4318-830F-D3DF4C7C14FD}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK20-voltage</Property>
	<Property Name="varPersistentID:{1F212298-71CD-42E8-8CBC-7FD88EB5B15C}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK18-current</Property>
	<Property Name="varPersistentID:{1F9C9EB1-CE1A-426B-93D1-E09FDE2917DA}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK05-AC</Property>
	<Property Name="varPersistentID:{1FCF4040-2C38-481E-874D-60EFF0A28041}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK10-Voltage-set</Property>
	<Property Name="varPersistentID:{205A4BB1-54DF-4745-9B2E-C61BFFFCF4E9}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK26-HV</Property>
	<Property Name="varPersistentID:{24CDC17B-F79E-49B9-BE04-882B154E17D2}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK03-voltage</Property>
	<Property Name="varPersistentID:{252296C1-85A1-4377-A25E-988251660547}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK26-Status</Property>
	<Property Name="varPersistentID:{25FBA710-AA56-4AD0-A117-9E6065363956}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK07-AC</Property>
	<Property Name="varPersistentID:{27C6F137-2C47-46DB-9241-BE5179634AB8}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK28-Status</Property>
	<Property Name="varPersistentID:{2B57E504-FC85-4F1D-A4A0-F450D21DA30C}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK03-current</Property>
	<Property Name="varPersistentID:{30C682EC-EBD0-4545-A2B7-39BB23BEC186}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK19-Status</Property>
	<Property Name="varPersistentID:{35FCE173-8D96-4BF1-9647-7EBB05A5CA9D}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK20-Status</Property>
	<Property Name="varPersistentID:{36E1F35C-AE63-4197-8767-939EC6782A63}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK15-current</Property>
	<Property Name="varPersistentID:{3B5B3299-5CA0-4017-9BB1-462C362C7E19}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK22-current</Property>
	<Property Name="varPersistentID:{3BF1876D-F620-4C09-A184-D311EF12F176}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK07-current</Property>
	<Property Name="varPersistentID:{3E1B0C77-B354-47FE-ACC8-1D6422604E4C}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK25-AC</Property>
	<Property Name="varPersistentID:{3F1E62FE-11F7-4BCA-8FE3-A6EA9A3D6B48}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK05-voltage</Property>
	<Property Name="varPersistentID:{3FA36B6D-AFC9-4989-898F-E0A84EA80683}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK27-AC</Property>
	<Property Name="varPersistentID:{40826D4E-5634-4E4D-968E-A5606D1E73C3}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK22-HV</Property>
	<Property Name="varPersistentID:{40B057EE-AB2A-4CF5-B93D-18C01CCD1D7E}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK25-current</Property>
	<Property Name="varPersistentID:{412D99CF-7B19-46AA-9F53-82AC996DD7BA}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK18-Voltage-set</Property>
	<Property Name="varPersistentID:{4273B511-115D-4189-8BD7-144CFB0480A6}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK11-AC</Property>
	<Property Name="varPersistentID:{44920493-BF32-4FBB-B622-D62E2FF2BB66}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK18-HV</Property>
	<Property Name="varPersistentID:{47DA2081-27B4-48F8-BBA8-865247A2B134}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK02-current</Property>
	<Property Name="varPersistentID:{495373A1-1C1E-4D55-802E-C93453162A24}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK10-AC</Property>
	<Property Name="varPersistentID:{49C1A9FE-9E09-4A35-B691-50914C42A272}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK22-AC</Property>
	<Property Name="varPersistentID:{4AF6A51F-06D3-4E7C-9791-9BFFFB24DD15}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK23-voltage</Property>
	<Property Name="varPersistentID:{4F148D6B-8586-460B-B9F3-86785AB00AA3}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK04-voltage</Property>
	<Property Name="varPersistentID:{4F166B0C-3580-4AD1-9A0D-4B699D4B1CD6}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK18-Status</Property>
	<Property Name="varPersistentID:{506BB8FA-646D-41DB-A3F8-C8C0C1FF873B}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK02-AC</Property>
	<Property Name="varPersistentID:{53FD2A1E-C4B4-4988-B428-FB2BD2A07395}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK09-Status</Property>
	<Property Name="varPersistentID:{58B91E41-C2FC-4070-84F6-F01A395C3609}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK24-HV</Property>
	<Property Name="varPersistentID:{59013368-4A74-4692-AEF8-B3B7487E8FD9}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK27-voltage</Property>
	<Property Name="varPersistentID:{59E240C1-467E-4CF1-8588-AA8EA39FB92D}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK26-AC</Property>
	<Property Name="varPersistentID:{5B102A01-4330-4C4E-8AE9-1C2870C5FA5D}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK17-HV</Property>
	<Property Name="varPersistentID:{5E2C9741-9D25-454C-85EF-3677CED3D09D}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK06-Voltage-set</Property>
	<Property Name="varPersistentID:{608A576B-EB93-4023-BF33-5684527E0D72}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK16-Voltage-set</Property>
	<Property Name="varPersistentID:{624A069F-BEA0-4117-A237-A1CB2020B5EE}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK21-AC</Property>
	<Property Name="varPersistentID:{625FE0B7-2FA8-461D-A521-77B09A700D52}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK20-AC</Property>
	<Property Name="varPersistentID:{6376A4A0-5A90-4FD5-A421-66F93B61EDB9}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK02-Voltage-set</Property>
	<Property Name="varPersistentID:{6427C5F4-B31B-4664-A0BC-8B9894924D20}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK27-Status</Property>
	<Property Name="varPersistentID:{65D45BD2-018E-4EEB-9C4F-59E793B991B8}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK27-HV</Property>
	<Property Name="varPersistentID:{67099062-F9A7-4FD1-922C-F49B708FAD8B}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK16-voltage</Property>
	<Property Name="varPersistentID:{6861F74F-AD0D-464B-A1F8-2FEAB8448BA5}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK05-Status</Property>
	<Property Name="varPersistentID:{6913FF8F-22CA-499B-96C6-DA5563BA677C}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK13-Voltage-set</Property>
	<Property Name="varPersistentID:{6B81E325-598A-497F-A802-A15B11F2B6FF}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK07-Status</Property>
	<Property Name="varPersistentID:{6C03BB21-A5FF-443D-9823-95BDAF42475B}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK13-Status</Property>
	<Property Name="varPersistentID:{6D52A244-3B1A-4BA8-9BD0-FF609CA68361}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK15-Voltage-set</Property>
	<Property Name="varPersistentID:{6E54AB37-B62C-469A-AAD0-EAA451779D88}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK25-voltage</Property>
	<Property Name="varPersistentID:{6F8691D3-675E-47C0-ADE5-A3A4B67BB4B7}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK01-AC</Property>
	<Property Name="varPersistentID:{700CFC7B-1F09-4249-A9CD-DA092B8C69D4}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK22-Status</Property>
	<Property Name="varPersistentID:{70B18C2C-58ED-4B1D-9E25-70B3CA52A792}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK22-voltage</Property>
	<Property Name="varPersistentID:{73858687-672A-444C-8DC2-3E86EBB71FBE}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK12-Voltage-set</Property>
	<Property Name="varPersistentID:{73D1DDBC-7ECC-4595-98A6-0C951F805B24}" Type="Ref">/My Computer/NDCXII controls main/Shared variables/Injector vacuum  system.lvlib/Injector vacuum status</Property>
	<Property Name="varPersistentID:{74FA72B6-DE83-452E-847C-5323A62DC147}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK01-Status</Property>
	<Property Name="varPersistentID:{7747A2C0-2ECE-44F8-A57A-065026DC7CFE}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK08-Status</Property>
	<Property Name="varPersistentID:{79B8448F-AD7A-4992-A46C-5AB3C527220F}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK21-Status</Property>
	<Property Name="varPersistentID:{7A62397F-FC7C-47B6-8783-6C3738E02F90}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK15-AC</Property>
	<Property Name="varPersistentID:{7B5F98C8-8517-4D32-B2B1-D01E1FB9519C}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK11-current</Property>
	<Property Name="varPersistentID:{7B92CD2C-096A-460C-B506-54F6ABFA4D3B}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK14-current</Property>
	<Property Name="varPersistentID:{7C2B3783-01E9-479F-A25E-53D6BA9A68F7}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK09-current</Property>
	<Property Name="varPersistentID:{7DDC2739-31E8-44F6-A452-C42844EB38C8}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK06-current</Property>
	<Property Name="varPersistentID:{7FF8F7B5-A320-4580-A375-D00F67747011}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK10-Status</Property>
	<Property Name="varPersistentID:{817CEEA4-E05D-47EB-B8CF-7CDF4493E5C6}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK21-voltage</Property>
	<Property Name="varPersistentID:{83019FF0-6A04-4EF4-AAEC-08AAF2C9B682}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK20-HV</Property>
	<Property Name="varPersistentID:{867BC30B-311E-41E9-B5C8-AE053EA3CAB5}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK25-Voltage-set</Property>
	<Property Name="varPersistentID:{8827172B-902F-45D9-8B54-09DEF118EEBC}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK01-current</Property>
	<Property Name="varPersistentID:{885A5D22-FC48-445C-B6D5-D06674E968DC}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK04-AC</Property>
	<Property Name="varPersistentID:{8887C856-4EEE-4D44-878C-3E56CDB2C014}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK24-Voltage-set</Property>
	<Property Name="varPersistentID:{88937922-0B3A-4F19-9FE2-C1A8126216BA}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK28-Voltage-set</Property>
	<Property Name="varPersistentID:{892F87E1-50AC-4036-A1C8-E0952C00A826}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK09-AC</Property>
	<Property Name="varPersistentID:{8BCFA54B-B3CD-4011-8ED5-D21BCBEF1332}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK16-AC</Property>
	<Property Name="varPersistentID:{8CA09BB4-7DB1-4F45-9153-140338626E7B}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK21-HV</Property>
	<Property Name="varPersistentID:{8D5F6B19-B205-485D-AAC6-69D915FF85F6}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK14-Status</Property>
	<Property Name="varPersistentID:{8F4FBAC6-5E2D-4034-A739-BB294B0EEDC7}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK23-AC</Property>
	<Property Name="varPersistentID:{906914C2-EE21-4356-951E-A7D24C0AA56B}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK27-current</Property>
	<Property Name="varPersistentID:{9091CE30-F652-44A1-A81C-403C30DE3D53}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK06-voltage</Property>
	<Property Name="varPersistentID:{91B86B99-500C-4692-B627-126D7A89B18E}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK24-Status</Property>
	<Property Name="varPersistentID:{92681E59-886E-47F5-8B45-22320439A179}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK17-Voltage-set</Property>
	<Property Name="varPersistentID:{92B89E96-4FBE-4B4B-A162-AFA4B948EEF1}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK17-AC</Property>
	<Property Name="varPersistentID:{999BC9C6-B3B5-42A2-810A-566B3FA6502E}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK09-Voltage-set</Property>
	<Property Name="varPersistentID:{9ECD762D-A779-4C8C-9D87-DEB86408C986}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK17-voltage</Property>
	<Property Name="varPersistentID:{9F6452FE-5BAB-4387-8347-9FEE913E150A}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK18-AC</Property>
	<Property Name="varPersistentID:{A0117B51-4EDA-4AB1-9415-735E912D5F06}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK05-HV</Property>
	<Property Name="varPersistentID:{A0E1C613-32EC-45B7-BFA9-4ED63ED2D418}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK06-AC</Property>
	<Property Name="varPersistentID:{A1B61D1A-C83C-44EF-B010-0DBCDCD2A5D7}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK23-current</Property>
	<Property Name="varPersistentID:{A2846325-4EA7-492A-879A-2326CA21EB83}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK25-Status</Property>
	<Property Name="varPersistentID:{A441A62A-3900-421B-AD77-ED7C12CDF6F0}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK01-Voltage-set</Property>
	<Property Name="varPersistentID:{A520065D-68DE-4B5E-BB72-153FBBECD951}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK26-Voltage-set</Property>
	<Property Name="varPersistentID:{A520C7E5-AEA4-4592-B8E6-4342D9872EC0}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK03-Voltage-set</Property>
	<Property Name="varPersistentID:{A7B869D8-2CF9-4BB1-ACAA-231830B10D00}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK28-HV</Property>
	<Property Name="varPersistentID:{A96E5F45-BD19-412C-AC92-7FB69FB56086}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK09-HV</Property>
	<Property Name="varPersistentID:{A9E25A73-5D43-4C8F-BF60-6F9FC2E9FCEA}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK08-Voltage-set</Property>
	<Property Name="varPersistentID:{AAC4465D-07B9-42CF-A725-C337BC62E167}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK21-Voltage-set</Property>
	<Property Name="varPersistentID:{AC049153-3C12-4975-B284-2C6F8510B6C6}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK11-Status</Property>
	<Property Name="varPersistentID:{ACF2FC7F-B7F9-4359-A320-A0400FB56CBB}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK02-Status</Property>
	<Property Name="varPersistentID:{AE58660D-BE86-46C7-86A1-F8C8A1F22FAC}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK23-Voltage-set</Property>
	<Property Name="varPersistentID:{AEAD0246-08BD-49A7-BAA2-4BB48E73927B}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK04-Status</Property>
	<Property Name="varPersistentID:{AF307A97-8AB3-4D08-A94B-A5C4E975BDB1}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK07-HV</Property>
	<Property Name="varPersistentID:{B0D091E4-6749-47CC-BDF1-764B20546EFE}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK06-HV</Property>
	<Property Name="varPersistentID:{B15E3164-3506-4A62-BB6B-A9F40B847CA8}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK22-Voltage-set</Property>
	<Property Name="varPersistentID:{B4159827-A3B0-4E4A-A4A2-011993EC8E17}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK14-voltage</Property>
	<Property Name="varPersistentID:{B676C35F-912A-4CF3-930A-F994B1AA8878}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK12-Status</Property>
	<Property Name="varPersistentID:{B72765A9-C6D7-4046-A3EE-B931A40FCF8B}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK05-Voltage-set</Property>
	<Property Name="varPersistentID:{B9A8E268-8DC0-4E89-9083-70C874311C95}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK12-current</Property>
	<Property Name="varPersistentID:{BB117A9E-AC51-4D14-9FD1-5432D3172AF3}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK14-AC</Property>
	<Property Name="varPersistentID:{C626AF63-E37F-4F68-8B51-70220C08E8F1}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK11-voltage</Property>
	<Property Name="varPersistentID:{C7375FFA-FFEC-46CE-9786-5167C00F6347}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK16-current</Property>
	<Property Name="varPersistentID:{CBA16D02-AC11-482D-80A9-F3BC50F9DB5F}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK02-HV</Property>
	<Property Name="varPersistentID:{CBA6192D-9784-4C0D-BF58-5B67D64A88DC}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK03-Status</Property>
	<Property Name="varPersistentID:{CCE4766C-8E34-4C9C-8DBF-D235518D7BBC}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK09-voltage</Property>
	<Property Name="varPersistentID:{CE183718-B3E7-4FAB-B692-DBC40E0CDA05}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK19-AC</Property>
	<Property Name="varPersistentID:{CF988EEE-7659-4289-BE70-9F521FFACFC4}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK13-AC</Property>
	<Property Name="varPersistentID:{D0FBA9EB-37CA-4E01-9167-F412A825562E}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK23-Status</Property>
	<Property Name="varPersistentID:{D12EBF24-A797-4134-A88D-20815DA253D6}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK19-voltage</Property>
	<Property Name="varPersistentID:{D2CD77A9-20EE-4AA4-B946-84D39F24D6B3}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK26-voltage</Property>
	<Property Name="varPersistentID:{D4A3DDF1-EFFB-4A24-93F1-388001B800DA}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK17-current</Property>
	<Property Name="varPersistentID:{D501CC99-5FB1-4A07-8064-9EE41B13E404}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK07-Voltage-set</Property>
	<Property Name="varPersistentID:{D56A8961-B589-44DC-BB25-A3D37BF29BE3}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK10-voltage</Property>
	<Property Name="varPersistentID:{D5D9824A-ED08-4ED8-9AAD-17A099220B89}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK14-Voltage-set</Property>
	<Property Name="varPersistentID:{D70EAD8D-7E86-43BD-9BE6-DDA847072BD3}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK08-AC</Property>
	<Property Name="varPersistentID:{D73253CB-207F-492A-A68F-CA71488C464C}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK04-HV</Property>
	<Property Name="varPersistentID:{D830EBE8-8D73-4A73-9B88-05749DB304E2}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK27-Voltage-set</Property>
	<Property Name="varPersistentID:{DB4212EA-63EE-4EF5-8CE2-C9A7613AF254}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK24-current</Property>
	<Property Name="varPersistentID:{DCB1D75F-2739-4290-A18E-ED6D6A32B719}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK03-HV</Property>
	<Property Name="varPersistentID:{DD26DE49-8089-4CA6-BF01-D2E6630119EE}" Type="Ref">/My Computer/NDCXII controls main/Shared variables/Power supply setpoints.lvlib/Injector and cell 1-7 HVPS setpoints</Property>
	<Property Name="varPersistentID:{DDD030CF-351B-4F37-B0FE-713A781637E6}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK13-voltage</Property>
	<Property Name="varPersistentID:{DDDF43A2-9B87-4D0B-BEB9-F4F72B83FBDA}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK19-Voltage-set</Property>
	<Property Name="varPersistentID:{DDED37F3-5AAF-483A-82B7-B1D28D4C22B2}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK13-current</Property>
	<Property Name="varPersistentID:{E0D777CB-38B8-44F8-9D64-B102BB26D8BB}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK21-current</Property>
	<Property Name="varPersistentID:{E2B7739E-C0E6-4CB7-9E4E-18E5818C88F9}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK11-Voltage-set</Property>
	<Property Name="varPersistentID:{E3617386-CAC4-44FF-8D55-35B1EA122F93}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK01-voltage</Property>
	<Property Name="varPersistentID:{E5FB8BE9-6D71-40A9-9D72-E29A7B3A5E2D}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK15-voltage</Property>
	<Property Name="varPersistentID:{E663FA20-08FF-4AD5-8925-A9F06FED8526}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK06-Status</Property>
	<Property Name="varPersistentID:{E8114F05-336D-4CFD-B52D-362A325B40D3}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK26-current</Property>
	<Property Name="varPersistentID:{E8A583E2-2F8B-4950-8C65-11763FAB513D}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK23-HV</Property>
	<Property Name="varPersistentID:{EA21209D-3BAA-46E4-995C-35FBEA9E25F2}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK20-Voltage-set</Property>
	<Property Name="varPersistentID:{EB4427E1-60F8-4E74-BD30-DB4C61944F30}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK24-AC</Property>
	<Property Name="varPersistentID:{EB935F10-67F6-4BEB-9862-AFDDE4C0ADFB}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK08-current</Property>
	<Property Name="varPersistentID:{F1E40DF6-1577-4177-9365-F1100F3B01FA}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK18-voltage</Property>
	<Property Name="varPersistentID:{F497B7A8-AE84-43AE-AD58-A1A373D3369C}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK13-HV</Property>
	<Property Name="varPersistentID:{F808DA75-D9C9-4769-BD9D-720C492B4E1C}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK16-Status</Property>
	<Property Name="varPersistentID:{FA27AD2D-8C91-4C5E-B265-2B055B216645}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK10-current</Property>
	<Property Name="varPersistentID:{FC4733C4-8479-4471-A79C-2E878ACBB2DE}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK15-Status</Property>
	<Property Name="varPersistentID:{FE1A7FB1-106E-4614-B2E0-D55091659310}" Type="Ref">/My Computer/NDCXII controls main/ModbusVariables.lvlib/SRK11-HV</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="NDCXII controls main" Type="Folder">
			<Item Name="cassandra" Type="Folder">
				<Item Name="cassandra-binary-protocall-v2.aliases" Type="Document" URL="../cassandra/cassandra-binary-protocall-v2.aliases"/>
				<Item Name="cassandra-binary-protocall-v2.lvlps" Type="Document" URL="../cassandra/cassandra-binary-protocall-v2.lvlps"/>
				<Item Name="cassandra-binary-protocall-v2.lvproj" Type="Document" URL="../cassandra/cassandra-binary-protocall-v2.lvproj"/>
				<Item Name="cassandra-test.vi" Type="VI" URL="../cassandra/cassandra-test.vi"/>
				<Item Name="cluster-to-str.vi" Type="VI" URL="../cassandra/cluster-to-str.vi"/>
				<Item Name="CQL-close-DB.vi" Type="VI" URL="../cassandra/CQL-close-DB.vi"/>
				<Item Name="CQL-consistencylevel.ctl" Type="VI" URL="../cassandra/CQL-consistencylevel.ctl"/>
				<Item Name="CQL-conv-type-json.vi" Type="VI" URL="../cassandra/CQL-conv-type-json.vi"/>
				<Item Name="CQL-flags.ctl" Type="VI" URL="../cassandra/CQL-flags.ctl"/>
				<Item Name="CQL-frame.ctl" Type="VI" URL="../cassandra/CQL-frame.ctl"/>
				<Item Name="CQL-NDCXII-insert.vi" Type="VI" URL="../cassandra/CQL-NDCXII-insert.vi"/>
				<Item Name="CQL-NDCXII-read_sett.vi" Type="VI" URL="../cassandra/CQL-NDCXII-read_sett.vi"/>
				<Item Name="CQL-opcode.ctl" Type="VI" URL="../cassandra/CQL-opcode.ctl"/>
				<Item Name="CQL-open-DB.vi" Type="VI" URL="../cassandra/CQL-open-DB.vi"/>
				<Item Name="CQL-option.ctl" Type="VI" URL="../cassandra/CQL-option.ctl"/>
				<Item Name="CQL-query.vi" Type="VI" URL="../cassandra/CQL-query.vi"/>
				<Item Name="CQL-Read-basic-byte.vi" Type="VI" URL="../cassandra/CQL-Read-basic-byte.vi"/>
				<Item Name="CQL-Read-basic-bytes.vi" Type="VI" URL="../cassandra/CQL-Read-basic-bytes.vi"/>
				<Item Name="CQL-Read-basic-consistency.vi" Type="VI" URL="../cassandra/CQL-Read-basic-consistency.vi"/>
				<Item Name="CQL-Read-basic-inet.vi" Type="VI" URL="../cassandra/CQL-Read-basic-inet.vi"/>
				<Item Name="CQL-Read-basic-int.vi" Type="VI" URL="../cassandra/CQL-Read-basic-int.vi"/>
				<Item Name="CQL-Read-basic-long-string.vi" Type="VI" URL="../cassandra/CQL-Read-basic-long-string.vi"/>
				<Item Name="CQL-Read-basic-short-bytes.vi" Type="VI" URL="../cassandra/CQL-Read-basic-short-bytes.vi"/>
				<Item Name="CQL-Read-basic-short.vi" Type="VI" URL="../cassandra/CQL-Read-basic-short.vi"/>
				<Item Name="CQL-Read-basic-string-list.vi" Type="VI" URL="../cassandra/CQL-Read-basic-string-list.vi"/>
				<Item Name="CQL-Read-basic-string-map.vi" Type="VI" URL="../cassandra/CQL-Read-basic-string-map.vi"/>
				<Item Name="CQL-Read-basic-string-multimap.vi" Type="VI" URL="../cassandra/CQL-Read-basic-string-multimap.vi"/>
				<Item Name="CQL-Read-basic-string.vi" Type="VI" URL="../cassandra/CQL-Read-basic-string.vi"/>
				<Item Name="CQL-Read-basic-uuid.vi" Type="VI" URL="../cassandra/CQL-Read-basic-uuid.vi"/>
				<Item Name="CQL-read-data-from-byte-array.vi" Type="VI" URL="../cassandra/CQL-read-data-from-byte-array.vi"/>
				<Item Name="CQL-Read-DB-Bigint.vi" Type="VI" URL="../cassandra/CQL-Read-DB-Bigint.vi"/>
				<Item Name="CQL-read-header.vi" Type="VI" URL="../cassandra/CQL-read-header.vi"/>
				<Item Name="CQL-Read-query-result-metadata.vi" Type="VI" URL="../cassandra/CQL-Read-query-result-metadata.vi"/>
				<Item Name="CQL-Read-result-column-type.vi" Type="VI" URL="../cassandra/CQL-Read-result-column-type.vi"/>
				<Item Name="CQL-read-result.vi" Type="VI" URL="../cassandra/CQL-read-result.vi"/>
				<Item Name="CQL-request-response.ctl" Type="VI" URL="../cassandra/CQL-request-response.ctl"/>
				<Item Name="CQL-use-keyspace.vi" Type="VI" URL="../cassandra/CQL-use-keyspace.vi"/>
				<Item Name="CQL-Write-basic-long-string.vi" Type="VI" URL="../cassandra/CQL-Write-basic-long-string.vi"/>
				<Item Name="CQL-Write-basic-string-list.vi" Type="VI" URL="../cassandra/CQL-Write-basic-string-list.vi"/>
				<Item Name="CQL-Write-basic-string-map.vi" Type="VI" URL="../cassandra/CQL-Write-basic-string-map.vi"/>
				<Item Name="CQL-Write-basic-string.vi" Type="VI" URL="../cassandra/CQL-Write-basic-string.vi"/>
				<Item Name="query-flags.ctl" Type="VI" URL="../cassandra/query-flags.ctl"/>
				<Item Name="query-result.ctl" Type="VI" URL="../cassandra/query-result.ctl"/>
				<Item Name="test-cluster-to-str.vi" Type="VI" URL="../cassandra/test-cluster-to-str.vi"/>
				<Item Name="test.vi" Type="VI" URL="../cassandra/test.vi"/>
				<Item Name="waveform-to-str-array.vi" Type="VI" URL="../cassandra/waveform-to-str-array.vi"/>
			</Item>
			<Item Name="cassandra-NDCXII-interface" Type="Folder">
				<Item Name="2ChannelScopeData.ctl" Type="VI" URL="../cassandra-NDCXII-interface/2ChannelScopeData.ctl"/>
				<Item Name="8ChannelScopeData.ctl" Type="VI" URL="../cassandra-NDCXII-interface/8ChannelScopeData.ctl"/>
				<Item Name="Conv-JSON-to-ScopeSetting.vi" Type="VI" URL="../cassandra-NDCXII-interface/Conv-JSON-to-ScopeSetting.vi"/>
				<Item Name="Conv-JSON-To-TimingSetting.vi" Type="VI" URL="../cassandra-NDCXII-interface/Conv-JSON-To-TimingSetting.vi"/>
				<Item Name="Conv-ScopeSetting-To-JSON.vi" Type="VI" URL="../cassandra-NDCXII-interface/Conv-ScopeSetting-To-JSON.vi"/>
				<Item Name="Conv-TimingSetting-To-JSON.vi" Type="VI" URL="../cassandra-NDCXII-interface/Conv-TimingSetting-To-JSON.vi"/>
				<Item Name="NDCXII-comment-add-shot-number.vi" Type="VI" URL="../cassandra-NDCXII-interface/NDCXII-comment-add-shot-number.vi"/>
				<Item Name="NDCXII-CQL-Comment-schema.ctl" Type="VI" URL="../cassandra-NDCXII-interface/NDCXII-CQL-Comment-schema.ctl"/>
				<Item Name="NDCXII-CQL-Data-schema.ctl" Type="VI" URL="../cassandra-NDCXII-interface/NDCXII-CQL-Data-schema.ctl"/>
				<Item Name="NDCXII-CQL-insert-comment.vi" Type="VI" URL="../cassandra-NDCXII-interface/NDCXII-CQL-insert-comment.vi"/>
				<Item Name="NDCXII-CQL-insert-data.vi" Type="VI" URL="../cassandra-NDCXII-interface/NDCXII-CQL-insert-data.vi"/>
				<Item Name="NDCXII-CQL-insert-setting.vi" Type="VI" URL="../cassandra-NDCXII-interface/NDCXII-CQL-insert-setting.vi"/>
				<Item Name="NDCXII-CQL-insert-shot.vi" Type="VI" URL="../cassandra-NDCXII-interface/NDCXII-CQL-insert-shot.vi"/>
				<Item Name="NDCXII-CQL-insert-time-device.vi" Type="VI" URL="../cassandra-NDCXII-interface/NDCXII-CQL-insert-time-device.vi"/>
				<Item Name="NDCXII-CQL-read-device-setting-hash.vi" Type="VI" URL="../cassandra-NDCXII-interface/NDCXII-CQL-read-device-setting-hash.vi"/>
				<Item Name="NDCXII-CQL-read-setting.vi" Type="VI" URL="../cassandra-NDCXII-interface/NDCXII-CQL-read-setting.vi"/>
				<Item Name="NDCXII-CQL-Shot-schema.ctl" Type="VI" URL="../cassandra-NDCXII-interface/NDCXII-CQL-Shot-schema.ctl"/>
				<Item Name="NDCXII-CQL-trigger-device-load-setting.vi" Type="VI" URL="../cassandra-NDCXII-interface/NDCXII-CQL-trigger-device-load-setting.vi"/>
				<Item Name="ScopeDataToJSON.vi" Type="VI" URL="../cassandra-NDCXII-interface/ScopeDataToJSON.vi"/>
				<Item Name="ScopeSetting-DB-v1.ctl" Type="VI" URL="../cassandra-NDCXII-interface/ScopeSetting-DB-v1.ctl"/>
				<Item Name="ScopeSettings-display-v1.ctl" Type="VI" URL="../cassandra-NDCXII-interface/ScopeSettings-display-v1.ctl"/>
				<Item Name="TimingSetting-DB-v1.ctl" Type="VI" URL="../cassandra-NDCXII-interface/TimingSetting-DB-v1.ctl"/>
				<Item Name="TimingSetting-display-v1.ctl" Type="VI" URL="../cassandra-NDCXII-interface/TimingSetting-display-v1.ctl"/>
				<Item Name="DipoleSetting-DB-v1.ctl" Type="VI" URL="../cassandra-NDCXII-interface/DipoleSetting-DB-v1.ctl"/>
				<Item Name="SRKSetting-DB-v1.ctl" Type="VI" URL="../cassandra-NDCXII-interface/SRKSetting-DB-v1.ctl"/>
				<Item Name="Save-Cont-Data-DB.vi" Type="VI" URL="../cassandra-NDCXII-interface/Save-Cont-Data-DB.vi"/>
			</Item>
			<Item Name="Globals" Type="Folder">
				<Item Name="Vacuum system globals" Type="Folder">
					<Item Name="Vaccum system ion gauge global.vi" Type="VI" URL="../Globals/Vacuum system globals/Vaccum system ion gauge global.vi"/>
					<Item Name="Vacuum system boolean global.vi" Type="VI" URL="../Globals/Vacuum system globals/Vacuum system boolean global.vi"/>
				</Item>
				<Item Name="Variac control globals" Type="Folder">
					<Item Name="Boolean controls globals.vi" Type="VI" URL="../Globals/Variac control globals/Boolean controls globals.vi"/>
					<Item Name="Filament analog globals.vi" Type="VI" URL="../Globals/Variac control globals/Filament analog globals.vi"/>
					<Item Name="Filament boolean controls globals.vi" Type="VI" URL="../Globals/Variac control globals/Filament boolean controls globals.vi"/>
					<Item Name="Filament graph cluster global.vi" Type="VI" URL="../Globals/Variac control globals/Filament graph cluster global.vi"/>
					<Item Name="Ramp up timer enabled.vi" Type="VI" URL="../Globals/Variac control globals/Ramp up timer enabled.vi"/>
				</Item>
			</Item>
			<Item Name="Graph sub vi&apos;s" Type="Folder">
				<Item Name="NDCXII Graph.vi" Type="VI" URL="../Graph sub vi&apos;s/NDCXII Graph.vi"/>
				<Item Name="NDXII-save-vacuum-data.vi" Type="VI" URL="../Graph sub vi&apos;s/NDXII-save-vacuum-data.vi"/>
				<Item Name="update-Graph-12h-history.vi" Type="VI" URL="../Graph sub vi&apos;s/update-Graph-12h-history.vi"/>
			</Item>
			<Item Name="NI MAX configuration files" Type="Folder">
				<Item Name="configData.nce" Type="Document" URL="../NI MAX configuration files/configData.nce"/>
				<Item Name="modbus-variables.csv" Type="Document" URL="../NI MAX configuration files/modbus-variables.csv"/>
			</Item>
			<Item Name="Pictures" Type="Folder">
				<Item Name="Beamline diagnostic chamber top.JPG" Type="Document" URL="../Pictures/Beamline diagnostic chamber top.JPG"/>
				<Item Name="beamline side 2.JPG" Type="Document" URL="../Pictures/beamline side 2.JPG"/>
				<Item Name="beamline side.JPG" Type="Document" URL="../Pictures/beamline side.JPG"/>
				<Item Name="beamline top 1.JPG" Type="Document" URL="../Pictures/beamline top 1.JPG"/>
				<Item Name="beamline top 2.JPG" Type="Document" URL="../Pictures/beamline top 2.JPG"/>
				<Item Name="beamline top 3.JPG" Type="Document" URL="../Pictures/beamline top 3.JPG"/>
				<Item Name="Diagnostic chamber side.JPG" Type="Document" URL="../Pictures/Diagnostic chamber side.JPG"/>
				<Item Name="x60_jy_ndcxii_layout_01-1.jpg" Type="Document" URL="../Pictures/x60_jy_ndcxii_layout_01-1.jpg"/>
				<Item Name="x60_jy_ndcxii_layout_01-2.jpg" Type="Document" URL="../Pictures/x60_jy_ndcxii_layout_01-2.jpg"/>
				<Item Name="x60_jy_ndcxii_layout_01.jpg" Type="Document" URL="../Pictures/x60_jy_ndcxii_layout_01.jpg"/>
				<Item Name="x60_jy_ndcxii_layout_02-1.jpg" Type="Document" URL="../Pictures/x60_jy_ndcxii_layout_02-1.jpg"/>
				<Item Name="x60_jy_ndcxii_layout_02-2.jpg" Type="Document" URL="../Pictures/x60_jy_ndcxii_layout_02-2.jpg"/>
				<Item Name="x60_jy_ndcxii_layout_02-3.jpg" Type="Document" URL="../Pictures/x60_jy_ndcxii_layout_02-3.jpg"/>
				<Item Name="x60_jy_ndcxii_layout_02.jpg" Type="Document" URL="../Pictures/x60_jy_ndcxii_layout_02.jpg"/>
				<Item Name="x60_jy_ndcxii_layout_03-1.jpg" Type="Document" URL="../Pictures/x60_jy_ndcxii_layout_03-1.jpg"/>
				<Item Name="x60_jy_ndcxii_layout_03.jpg" Type="Document" URL="../Pictures/x60_jy_ndcxii_layout_03.jpg"/>
			</Item>
			<Item Name="Injector" Type="Folder">
				<Item Name="Injector-init-temp-readout.vi" Type="VI" URL="../Injector/Injector-init-temp-readout.vi"/>
				<Item Name="Injector-init-flow-readout.vi" Type="VI" URL="../Injector/Injector-init-flow-readout.vi"/>
				<Item Name="SourceControl.vi" Type="VI" URL="../Injector/SourceControl.vi"/>
			</Item>
			<Item Name="Power supplies" Type="Folder">
				<Item Name="Corrector coils" Type="Folder">
					<Item Name="Sub vi&apos;s" Type="Folder">
						<Item Name="Lambda Gensys controls mod 1.vi" Type="VI" URL="../Power supplies/Corrector coils/Sub vi&apos;s/Lambda Gensys controls mod 1.vi"/>
						<Item Name="Lambda Gensys Init.vi" Type="VI" URL="../Power supplies/Corrector coils/Sub vi&apos;s/Lambda Gensys Init.vi"/>
						<Item Name="Lambda Gensys read status.vi" Type="VI" URL="../Power supplies/Corrector coils/Sub vi&apos;s/Lambda Gensys read status.vi"/>
						<Item Name="Lambda Gensys set voltage.vi" Type="VI" URL="../Power supplies/Corrector coils/Sub vi&apos;s/Lambda Gensys set voltage.vi"/>
						<Item Name="Lambda Gensys turn on-off.vi" Type="VI" URL="../Power supplies/Corrector coils/Sub vi&apos;s/Lambda Gensys turn on-off.vi"/>
						<Item Name="Dipole-find-control.vi" Type="VI" URL="../Power supplies/Corrector coils/Sub vi&apos;s/Dipole-find-control.vi"/>
						<Item Name="Dipole-test-polarity-change.vi" Type="VI" URL="../Power supplies/Corrector coils/Sub vi&apos;s/Dipole-test-polarity-change.vi"/>
						<Item Name="Dipoles-set-polarity.vi" Type="VI" URL="../Power supplies/Corrector coils/Sub vi&apos;s/Dipoles-set-polarity.vi"/>
						<Item Name="Dipole-toggle-polarity-array.vi" Type="VI" URL="../Power supplies/Corrector coils/Sub vi&apos;s/Dipole-toggle-polarity-array.vi"/>
					</Item>
					<Item Name="CoilInput.ctl" Type="VI" URL="../Power supplies/Corrector coils/CoilInput.ctl"/>
					<Item Name="CoilOutput.ctl" Type="VI" URL="../Power supplies/Corrector coils/CoilOutput.ctl"/>
					<Item Name="Corrector coil supplies.vi" Type="VI" URL="../Power supplies/Corrector coils/Corrector coil supplies.vi"/>
				</Item>
				<Item Name="Power supply globals" Type="Folder">
					<Item Name="PRK03-05 reference global.vi" Type="VI" URL="../Power supplies/Power supply globals/PRK03-05 reference global.vi"/>
				</Item>
				<Item Name="NDCXII power supply control PRK03-05 Cassandra test.vi" Type="VI" URL="../Power supplies/NDCXII power supply control PRK03-05 Cassandra test.vi"/>
				<Item Name="NDCXII power supply control PRK03-05.vi" Type="VI" URL="../Power supplies/NDCXII power supply control PRK03-05.vi"/>
			</Item>
			<Item Name="Running" Type="Folder">
				<Item Name="NDCXII controls main 3-8-13.vi" Type="VI" URL="../Running/NDCXII controls main 3-8-13.vi"/>
				<Item Name="NDCXII Main Menu" Type="Document" URL="../Running/NDCXII Main Menu"/>
			</Item>
			<Item Name="Shared variables" Type="Folder">
				<Item Name="Injector vacuum  system.lvlib" Type="Library" URL="../Shared variables/Injector vacuum  system.lvlib"/>
				<Item Name="Power supply setpoints.lvlib" Type="Library" URL="../Shared variables/Power supply setpoints.lvlib"/>
			</Item>
			<Item Name="Solenoid pulser racks" Type="Folder">
				<Item Name="createLUT-bool.vi" Type="VI" URL="../Solenoid pulser racks/createLUT-bool.vi"/>
				<Item Name="Check-HV-status.vi" Type="VI" URL="../Solenoid pulser racks/Check-HV-status.vi"/>
				<Item Name="createLUT-u16.vi" Type="VI" URL="../Solenoid pulser racks/createLUT-u16.vi"/>
				<Item Name="createLUT.vi" Type="VI" URL="../Solenoid pulser racks/createLUT.vi"/>
				<Item Name="find-ind-ref-bool-array.vi" Type="VI" URL="../Solenoid pulser racks/find-ind-ref-bool-array.vi"/>
				<Item Name="find-ind-ref-i16.vi" Type="VI" URL="../Solenoid pulser racks/find-ind-ref-i16.vi"/>
				<Item Name="find-ind-ref.vi" Type="VI" URL="../Solenoid pulser racks/find-ind-ref.vi"/>
				<Item Name="Get SRK name.vi" Type="VI" URL="../Solenoid pulser racks/Get SRK name.vi"/>
				<Item Name="SolenoidTemperatureReadout.vi" Type="VI" URL="../SolenoidTemperatures/SolenoidTemperatureReadout.vi"/>
				<Item Name="Solenoid-control.vi" Type="VI" URL="../Solenoid pulser racks/Solenoid-control.vi"/>
				<Item Name="SRK-control.ctl" Type="VI" URL="../Solenoid pulser racks/SRK-control.ctl"/>
				<Item Name="SRK-inidicator.ctl" Type="VI" URL="../Solenoid pulser racks/SRK-inidicator.ctl"/>
			</Item>
			<Item Name="Typedefs" Type="Folder">
				<Item Name="SRK-LED-indicators.ctl" Type="VI" URL="../Typedefs/SRK-LED-indicators.ctl"/>
				<Item Name="TCP-connections.ctl" Type="VI" URL="../Typedefs/TCP-connections.ctl"/>
				<Item Name="Source-Settings.ctl" Type="VI" URL="../Typedefs/Source-Settings.ctl"/>
			</Item>
			<Item Name="Vacuum sub vi&apos;s" Type="Folder">
				<Item Name="PLC sub vi&apos;s" Type="Folder">
					<Item Name="Vacuum PLC read cluster v3.vi" Type="VI" URL="../Vacuum sub vi&apos;s/PLC sub vi&apos;s/Vacuum PLC read cluster v3.vi"/>
					<Item Name="Vacuum PLC write.vi" Type="VI" URL="../Vacuum sub vi&apos;s/PLC sub vi&apos;s/Vacuum PLC write.vi"/>
				</Item>
				<Item Name="Vacuum system panel 6 intercells 1-15-15.vi" Type="VI" URL="../Vacuum sub vi&apos;s/Vacuum system panel 6 intercells 1-15-15.vi"/>
			</Item>
			<Item Name="Variac sub vi&apos;s" Type="Folder">
				<Item Name="Check variac status startup.vi" Type="VI" URL="../Variac sub vi&apos;s/Check variac status startup.vi"/>
				<Item Name="HandleJogButtonUpDown.vi" Type="VI" URL="../Variac sub vi&apos;s/HandleJogButtonUpDown.vi"/>
				<Item Name="ramp down.vi" Type="VI" URL="../Variac sub vi&apos;s/ramp down.vi"/>
				<Item Name="ramp up.vi" Type="VI" URL="../Variac sub vi&apos;s/ramp up.vi"/>
				<Item Name="Variac control 3.vi" Type="VI" URL="../Variac sub vi&apos;s/Variac control 3.vi"/>
			</Item>
			<Item Name="ZMQ-events" Type="Folder">
				<Item Name="ZMQ-subscribe-to-shot-events.vi" Type="VI" URL="../ZMQ-events/ZMQ-subscribe-to-shot-events.vi"/>
				<Item Name="ZMQ-REP-load-hash.vi" Type="VI" URL="../ZMQ-events/ZMQ-REP-load-hash.vi"/>
			</Item>
			<Item Name="ModbusVariables.lvlib" Type="Library" URL="../ModbusVariables.lvlib"/>
			<Item Name="NDCXII controls.aliases" Type="Document" URL="../NDCXII controls.aliases"/>
			<Item Name="NDCXII controls.lvlps" Type="Document" URL="../NDCXII controls.lvlps"/>
			<Item Name="wait-ms-error.vi" Type="VI" URL="../wait-ms-error.vi"/>
		</Item>
		<Item Name="Injector-ps-input.vi" Type="VI" URL="../Injector/Injector-ps-input.vi"/>
		<Item Name="Injector-ps-output.vi" Type="VI" URL="../Injector/Injector-ps-output.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="instr.lib" Type="Folder">
				<Item Name="GFT9404_Auxiliary Delay Channels.ctl" Type="VI" URL="/&lt;instrlib&gt;/GreenfieldTechnology GFT9404/_Typedefs/GFT9404_Auxiliary Delay Channels.ctl"/>
				<Item Name="GFT9404_Precision Delay Channels.ctl" Type="VI" URL="/&lt;instrlib&gt;/GreenfieldTechnology GFT9404/_Typedefs/GFT9404_Precision Delay Channels.ctl"/>
				<Item Name="niScope trigger coupling.ctl" Type="VI" URL="/&lt;instrlib&gt;/niScope/Controls/niScope trigger coupling.ctl"/>
				<Item Name="niScope trigger slope.ctl" Type="VI" URL="/&lt;instrlib&gt;/niScope/Controls/niScope trigger slope.ctl"/>
				<Item Name="niScope trigger window mode.ctl" Type="VI" URL="/&lt;instrlib&gt;/niScope/Controls/niScope trigger window mode.ctl"/>
				<Item Name="niScope vertical coupling.ctl" Type="VI" URL="/&lt;instrlib&gt;/niScope/Controls/niScope vertical coupling.ctl"/>
			</Item>
			<Item Name="user.lib" Type="Folder">
				<Item Name="MD5 F function__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/md5/md5.llb/MD5 F function__ogtk.vi"/>
				<Item Name="MD5 FGHI functions__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/md5/md5.llb/MD5 FGHI functions__ogtk.vi"/>
				<Item Name="MD5 G function__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/md5/md5.llb/MD5 G function__ogtk.vi"/>
				<Item Name="MD5 H function__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/md5/md5.llb/MD5 H function__ogtk.vi"/>
				<Item Name="MD5 I function__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/md5/md5.llb/MD5 I function__ogtk.vi"/>
				<Item Name="MD5 Message Digest (Binary String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/md5/md5.llb/MD5 Message Digest (Binary String)__ogtk.vi"/>
				<Item Name="MD5 Message Digest (Hexadecimal String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/md5/md5.llb/MD5 Message Digest (Hexadecimal String)__ogtk.vi"/>
				<Item Name="MD5 Message Digest__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/md5/md5.llb/MD5 Message Digest__ogtk.vi"/>
				<Item Name="MD5 Padding__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/md5/md5.llb/MD5 Padding__ogtk.vi"/>
				<Item Name="MD5 ti__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/md5/md5.llb/MD5 ti__ogtk.vi"/>
				<Item Name="MD5 Unrecoverable character padding__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/md5/md5.llb/MD5 Unrecoverable character padding__ogtk.vi"/>
				<Item Name="Wait (ms)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/time/time.llb/Wait (ms)__ogtk.vi"/>
				<Item Name="MD5 Hash__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/md5/md5.llb/MD5 Hash__ogtk.vi"/>
			</Item>
			<Item Name="vi.lib" Type="Folder">
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="DAQmx Clear Task.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/task.llb/DAQmx Clear Task.vi"/>
				<Item Name="DAQmx Fill In Error Info.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/miscellaneous.llb/DAQmx Fill In Error Info.vi"/>
				<Item Name="DAQmx Read (Analog 1D DBL 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D DBL 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 1D DBL NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D DBL NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Analog 1D Wfm NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D Wfm NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Analog 1D Wfm NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D Wfm NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D DBL NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D DBL NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D I16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D I16 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D I32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D I32 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D U16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D U16 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D U32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D U32 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog DBL 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog DBL 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Analog Wfm 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog Wfm 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Analog Wfm 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog Wfm 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D DBL 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D DBL 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D Pulse Freq 1 Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D Pulse Freq 1 Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D Pulse Ticks 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D Pulse Ticks 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D Pulse Time 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D Pulse Time 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D U32 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D U32 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter DBL 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter DBL 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter Pulse Freq 1 Chan 1 Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter Pulse Freq 1 Chan 1 Samp).vi"/>
				<Item Name="DAQmx Read (Counter Pulse Ticks 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter Pulse Ticks 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter Pulse Time 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter Pulse Time 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter U32 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter U32 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D Bool 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Bool 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D Bool NChan 1Samp 1Line).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Bool NChan 1Samp 1Line).vi"/>
				<Item Name="DAQmx Read (Digital 1D U8 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U8 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U8 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U8 NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U16 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U16 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U16 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U16 NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U32 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U32 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U32 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U32 NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D Wfm NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Wfm NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D Wfm NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Wfm NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 2D Bool NChan 1Samp NLine).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 2D Bool NChan 1Samp NLine).vi"/>
				<Item Name="DAQmx Read (Digital 2D U8 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 2D U8 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 2D U16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 2D U16 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 2D U32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 2D U32 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital Bool 1Line 1Point).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital Bool 1Line 1Point).vi"/>
				<Item Name="DAQmx Read (Digital U8 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital U8 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital U16 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital U16 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital U32 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital U32 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital Wfm 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital Wfm 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital Wfm 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital Wfm 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Raw 1D I8).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D I8).vi"/>
				<Item Name="DAQmx Read (Raw 1D I16).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D I16).vi"/>
				<Item Name="DAQmx Read (Raw 1D I32).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D I32).vi"/>
				<Item Name="DAQmx Read (Raw 1D U8).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D U8).vi"/>
				<Item Name="DAQmx Read (Raw 1D U16).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D U16).vi"/>
				<Item Name="DAQmx Read (Raw 1D U32).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D U32).vi"/>
				<Item Name="DAQmx Read.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read.vi"/>
				<Item Name="DAQmx Write (Analog 1D DBL 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 1D DBL 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog 1D DBL NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 1D DBL NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog 1D Wfm NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 1D Wfm NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog 1D Wfm NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 1D Wfm NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog 2D DBL NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 2D DBL NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog 2D I16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 2D I16 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog 2D I32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 2D I32 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog 2D U16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 2D U16 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog DBL 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog DBL 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog Wfm 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog Wfm 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog Wfm 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog Wfm 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Frequency 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Frequency 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Frequency NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Frequency NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Ticks 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Ticks 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Time 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Time 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Time NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Time NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter 1DTicks NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1DTicks NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter Frequency 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter Frequency 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter Ticks 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter Ticks 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter Time 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter Time 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D Bool 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D Bool 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D Bool NChan 1Samp 1Line).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D Bool NChan 1Samp 1Line).vi"/>
				<Item Name="DAQmx Write (Digital 1D U8 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U8 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U8 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U8 NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U16 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U16 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U16 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U16 NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U32 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U32 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U32 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U32 NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D Wfm NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D Wfm NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D Wfm NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D Wfm NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 2D Bool NChan 1Samp NLine).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 2D Bool NChan 1Samp NLine).vi"/>
				<Item Name="DAQmx Write (Digital 2D U8 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 2D U8 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 2D U16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 2D U16 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 2D U32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 2D U32 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital Bool 1Line 1Point).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital Bool 1Line 1Point).vi"/>
				<Item Name="DAQmx Write (Digital U8 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital U8 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital U16 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital U16 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital U32 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital U32 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital Wfm 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital Wfm 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital Wfm 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital Wfm 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Raw 1D I8).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D I8).vi"/>
				<Item Name="DAQmx Write (Raw 1D I16).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D I16).vi"/>
				<Item Name="DAQmx Write (Raw 1D I32).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D I32).vi"/>
				<Item Name="DAQmx Write (Raw 1D U8).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D U8).vi"/>
				<Item Name="DAQmx Write (Raw 1D U16).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D U16).vi"/>
				<Item Name="DAQmx Write (Raw 1D U32).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D U32).vi"/>
				<Item Name="DAQmx Write.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="DTbl Digital Size.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Digital Size.vi"/>
				<Item Name="DTbl Uncompress Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Uncompress Digital.vi"/>
				<Item Name="DWDT Uncompress Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Uncompress Digital.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="FormatTime String.vi" Type="VI" URL="/&lt;vilib&gt;/express/express execution control/ElapsedTimeBlock.llb/FormatTime String.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="MB Ethernet Master Query (poly).vi" Type="VI" URL="/&lt;vilib&gt;/NI Modbus.llb/MB Ethernet Master Query (poly).vi"/>
				<Item Name="MB Ethernet Master Query Read Coils (poly).vi" Type="VI" URL="/&lt;vilib&gt;/NI Modbus.llb/MB Ethernet Master Query Read Coils (poly).vi"/>
				<Item Name="MB Ethernet Master Query Read Discrete Inputs (poly).vi" Type="VI" URL="/&lt;vilib&gt;/NI Modbus.llb/MB Ethernet Master Query Read Discrete Inputs (poly).vi"/>
				<Item Name="MB Ethernet Master Query Read Holding Registers (poly).vi" Type="VI" URL="/&lt;vilib&gt;/NI Modbus.llb/MB Ethernet Master Query Read Holding Registers (poly).vi"/>
				<Item Name="MB Ethernet Master Query Read Input Registers (poly).vi" Type="VI" URL="/&lt;vilib&gt;/NI Modbus.llb/MB Ethernet Master Query Read Input Registers (poly).vi"/>
				<Item Name="MB Ethernet Master Query Write Single Coil (poly).vi" Type="VI" URL="/&lt;vilib&gt;/NI Modbus.llb/MB Ethernet Master Query Write Single Coil (poly).vi"/>
				<Item Name="MB Ethernet Master Query Write Single Register (poly).vi" Type="VI" URL="/&lt;vilib&gt;/NI Modbus.llb/MB Ethernet Master Query Write Single Register (poly).vi"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="Stop Timed Structure.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/TimedLoop/scheduler/LVUserAPI/Stop Timed Structure.vi"/>
				<Item Name="subElapsedTime.vi" Type="VI" URL="/&lt;vilib&gt;/express/express execution control/ElapsedTimeBlock.llb/subElapsedTime.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="VariantType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/VariantDataType/VariantType.lvlib"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="zeromq.lvlib" Type="Library" URL="/&lt;vilib&gt;/addons/zeromq/zeromq.lvlib"/>
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="DAQmx Stop Task.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/task.llb/DAQmx Stop Task.vi"/>
				<Item Name="DAQmx Start Task.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/task.llb/DAQmx Start Task.vi"/>
				<Item Name="DAQmx Create Task.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/task.llb/DAQmx Create Task.vi"/>
				<Item Name="DAQmx Create Channel (AI-Strain-Rosette Strain Gage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Strain-Rosette Strain Gage).vi"/>
				<Item Name="DAQmx Create Channel (AI-Velocity-IEPE Sensor).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Velocity-IEPE Sensor).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (AI-Force-IEPE Sensor).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Force-IEPE Sensor).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Force-IEPE Sensor).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Force-IEPE Sensor).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Torque-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Torque-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Pressure-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Pressure-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Force-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Force-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (AI-Torque-Bridge-Polynomial).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Torque-Bridge-Polynomial).vi"/>
				<Item Name="DAQmx Create Channel (AI-Torque-Bridge-Table).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Torque-Bridge-Table).vi"/>
				<Item Name="DAQmx Create Channel (AI-Torque-Bridge-Two-Point-Linear).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Torque-Bridge-Two-Point-Linear).vi"/>
				<Item Name="DAQmx Create Channel (AI-Pressure-Bridge-Polynomial).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Pressure-Bridge-Polynomial).vi"/>
				<Item Name="DAQmx Create Channel (AI-Pressure-Bridge-Table).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Pressure-Bridge-Table).vi"/>
				<Item Name="DAQmx Create Channel (AI-Pressure-Bridge-Two-Point-Linear).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Pressure-Bridge-Two-Point-Linear).vi"/>
				<Item Name="DAQmx Create Channel (AI-Force-Bridge-Two-Point-Linear).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Force-Bridge-Two-Point-Linear).vi"/>
				<Item Name="DAQmx Create Channel (AI-Force-Bridge-Table).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Force-Bridge-Table).vi"/>
				<Item Name="DAQmx Create Channel (AI-Force-Bridge-Polynomial).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Force-Bridge-Polynomial).vi"/>
				<Item Name="DAQmx Create Channel (AI-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (CI-Pulse Ticks).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Pulse Ticks).vi"/>
				<Item Name="DAQmx Create Channel (CI-Pulse Time).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Pulse Time).vi"/>
				<Item Name="DAQmx Create Channel (CI-Pulse Freq).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Pulse Freq).vi"/>
				<Item Name="DAQmx Create Channel (AI-Position-EddyCurrentProxProbe).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Position-EddyCurrentProxProbe).vi"/>
				<Item Name="DAQmx Create Channel (AI-Current-RMS).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Current-RMS).vi"/>
				<Item Name="DAQmx Create Channel (AI-Voltage-RMS).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Voltage-RMS).vi"/>
				<Item Name="DAQmx Create Channel (AO-Current-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AO-Current-Basic).vi"/>
				<Item Name="DAQmx Create Channel (CI-GPS Timestamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-GPS Timestamp).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Sound Pressure-Microphone).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Sound Pressure-Microphone).vi"/>
				<Item Name="DAQmx Create Channel (AI-Sound Pressure-Microphone).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Sound Pressure-Microphone).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Temperature-Thermocouple).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Temperature-Thermocouple).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Voltage-Custom with Excitation).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Voltage-Custom with Excitation).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Voltage-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Voltage-Basic).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Temperature-Thermistor-Vex).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Temperature-Thermistor-Vex).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Temperature-Thermistor-Iex).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Temperature-Thermistor-Iex).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Temperature-RTD).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Temperature-RTD).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Strain-Strain Gage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Strain-Strain Gage).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Resistance).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Resistance).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Position-RVDT).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Position-RVDT).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Position-LVDT).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Position-LVDT).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Current-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Current-Basic).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Acceleration-Accelerometer).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Acceleration-Accelerometer).vi"/>
				<Item Name="DAQmx Create Channel (CI-Position-Linear Encoder).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Position-Linear Encoder).vi"/>
				<Item Name="DAQmx Create Channel (CI-Position-Angular Encoder).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Position-Angular Encoder).vi"/>
				<Item Name="DAQmx Create Channel (AI-Acceleration-Accelerometer).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Acceleration-Accelerometer).vi"/>
				<Item Name="DAQmx Create Channel (CI-Two Edge Separation).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Two Edge Separation).vi"/>
				<Item Name="DAQmx Create Channel (AI-Position-RVDT).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Position-RVDT).vi"/>
				<Item Name="DAQmx Create Channel (AI-Position-LVDT).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Position-LVDT).vi"/>
				<Item Name="DAQmx Create Channel (CO-Pulse Generation-Ticks).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CO-Pulse Generation-Ticks).vi"/>
				<Item Name="DAQmx Create Channel (CO-Pulse Generation-Time).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CO-Pulse Generation-Time).vi"/>
				<Item Name="DAQmx Create Channel (CO-Pulse Generation-Frequency).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CO-Pulse Generation-Frequency).vi"/>
				<Item Name="DAQmx Create Channel (AI-Frequency-Voltage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Frequency-Voltage).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-Built-in Sensor).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-Built-in Sensor).vi"/>
				<Item Name="DAQmx Create Channel (AI-Strain-Strain Gage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Strain-Strain Gage).vi"/>
				<Item Name="DAQmx Create Channel (AI-Current-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Current-Basic).vi"/>
				<Item Name="DAQmx Create Channel (CI-Semi Period).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Semi Period).vi"/>
				<Item Name="DAQmx Create Channel (CI-Pulse Width).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Pulse Width).vi"/>
				<Item Name="DAQmx Create Channel (CI-Count Edges).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Count Edges).vi"/>
				<Item Name="DAQmx Create Channel (CI-Period).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Period).vi"/>
				<Item Name="DAQmx Create Channel (CI-Frequency).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Frequency).vi"/>
				<Item Name="DAQmx Create Channel (DO-Digital Output).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (DO-Digital Output).vi"/>
				<Item Name="DAQmx Create Channel (DI-Digital Input).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (DI-Digital Input).vi"/>
				<Item Name="DAQmx Create Channel (AO-FuncGen).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AO-FuncGen).vi"/>
				<Item Name="DAQmx Create Channel (AO-Voltage-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AO-Voltage-Basic).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-Thermistor-Vex).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-Thermistor-Vex).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-Thermistor-Iex).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-Thermistor-Iex).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-RTD).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-RTD).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-Thermocouple).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-Thermocouple).vi"/>
				<Item Name="DAQmx Create Channel (AI-Resistance).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Resistance).vi"/>
				<Item Name="DAQmx Create Channel (AI-Voltage-Custom with Excitation).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Voltage-Custom with Excitation).vi"/>
				<Item Name="DAQmx Create Channel (AI-Voltage-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Voltage-Basic).vi"/>
				<Item Name="DAQmx Create Virtual Channel.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Virtual Channel.vi"/>
				<Item Name="DAQmx Create Channel (CI-Duty Cycle).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Duty Cycle).vi"/>
				<Item Name="DAQmx Read (Counter 1D DBL NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D DBL NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter 1D U32 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D U32 NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter 2D DBL NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 2D DBL NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 2D U32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 2D U32 NChan NSamp).vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="MB Modbus Command.ctl" Type="VI" URL="/&lt;vilib&gt;/NI Modbus.llb/MB Modbus Command.ctl"/>
				<Item Name="MB Modbus Data Unit.ctl" Type="VI" URL="/&lt;vilib&gt;/NI Modbus.llb/MB Modbus Data Unit.ctl"/>
				<Item Name="MB Ethernet String to Modbus Data Unit.vi" Type="VI" URL="/&lt;vilib&gt;/NI Modbus.llb/MB Ethernet String to Modbus Data Unit.vi"/>
				<Item Name="MB Ethernet Receive.vi" Type="VI" URL="/&lt;vilib&gt;/NI Modbus.llb/MB Ethernet Receive.vi"/>
				<Item Name="MB Ethernet Transmit.vi" Type="VI" URL="/&lt;vilib&gt;/NI Modbus.llb/MB Ethernet Transmit.vi"/>
				<Item Name="MB Decode Data.vi" Type="VI" URL="/&lt;vilib&gt;/NI Modbus.llb/MB Decode Data.vi"/>
				<Item Name="MB Modbus Command to Data Unit.vi" Type="VI" URL="/&lt;vilib&gt;/NI Modbus.llb/MB Modbus Command to Data Unit.vi"/>
				<Item Name="MB Ethernet Master Query.vi" Type="VI" URL="/&lt;vilib&gt;/NI Modbus.llb/MB Ethernet Master Query.vi"/>
				<Item Name="MB Ethernet Master Query Read Exception Status (poly).vi" Type="VI" URL="/&lt;vilib&gt;/NI Modbus.llb/MB Ethernet Master Query Read Exception Status (poly).vi"/>
				<Item Name="MB Ethernet Master Query Write Multiple Registers (poly).vi" Type="VI" URL="/&lt;vilib&gt;/NI Modbus.llb/MB Ethernet Master Query Write Multiple Registers (poly).vi"/>
				<Item Name="MB Ethernet Master Query Write Multiple Coils (poly).vi" Type="VI" URL="/&lt;vilib&gt;/NI Modbus.llb/MB Ethernet Master Query Write Multiple Coils (poly).vi"/>
				<Item Name="DAQmx Reset Device.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/system.llb/DAQmx Reset Device.vi"/>
				<Item Name="DAQmx Self-Test Device.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/system.llb/DAQmx Self-Test Device.vi"/>
				<Item Name="DAQmx Create Channel (CI-Velocity-Angular).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Velocity-Angular).vi"/>
				<Item Name="DAQmx Create Channel (CI-Velocity-Linear).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Velocity-Linear).vi"/>
				<Item Name="DAQmx Create Channel (AI-Acceleration-4 Wire DC Voltage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Acceleration-4 Wire DC Voltage).vi"/>
				<Item Name="DAQmx Create Channel (AI-Acceleration-Charge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Acceleration-Charge).vi"/>
				<Item Name="DAQmx Create Channel (AI-Charge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Charge).vi"/>
				<Item Name="DAQmx Read (Analog Wfm 1Chan NSamp Duration).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog Wfm 1Chan NSamp Duration).vi"/>
				<Item Name="DAQmx Read (Analog 1D Wfm NChan NSamp Duration).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D Wfm NChan NSamp Duration).vi"/>
				<Item Name="DAQmx Read (Digital Wfm 1Chan NSamp Duration).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital Wfm 1Chan NSamp Duration).vi"/>
				<Item Name="DAQmx Read (Digital 1D Wfm NChan NSamp Duration).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Wfm NChan NSamp Duration).vi"/>
			</Item>
			<Item Name="lvalarms.dll" Type="Document" URL="lvalarms.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="nilvaiu.dll" Type="Document" URL="nilvaiu.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
