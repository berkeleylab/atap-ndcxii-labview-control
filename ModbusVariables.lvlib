﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="15008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5Q&lt;/07RB7W!,&lt;'&amp;&lt;9+K1,7Q,&lt;)%N&lt;!NMA3X)DW?-RJ(JQ"I\%%Z,(@`BA#==ZB3RN;]28_,V7@PWW`:R`&gt;HV*SU_WE@\N_XF[3:^^TX\+2YP)D7K6;G-RV3P)R`ZS%=_]J'XP/5N&lt;XH,7V\SEJ?]Z#5P?=J4HP+5JTTFWS%0?=B$DD1G(R/.1==!IT.+D)`B':\B'2Z@9XC':XC':XBUC?%:HO%:HO&amp;R7QT0]!T0]!S0I4&lt;*&lt;)?=:XA-(]X40-X40-VDSGC?"GC4N9(&lt;)"D2,L;4ZGG?ZH%;T&gt;-]T&gt;-]T?.S.%`T.%`T.)^&lt;NF8J4@-YZ$S'C?)JHO)JHO)R&gt;"20]220]230[;*YCK=ASI2F=)1I.Z5/Z5PR&amp;)^@54T&amp;5TT&amp;5TQO&lt;5_INJ6Z;"[(H#&gt;ZEC&gt;ZEC&gt;Z$"(*ETT*ETT*9^B)HO2*HO2*(F.&amp;]C20]C2)GN4UE1:,.[:/+5A?0^NOS?UJ^3&lt;*\9B9GT@7JISVW7*NIFC&lt;)^:$D`5Q9TWE7)M@;V&amp;D,6;M29DVR]6#R],%GC47T9_/=@&gt;Z5V&gt;V57&gt;V5E&gt;V5(OV?^T[FTP?\`?YX7ZRP6\D=LH%_8S/U_E5R_-R$I&gt;$\0@\W/VW&lt;[_"\Y[X&amp;],0^^+,]T_J&gt;`J@_B_]'_.T`$KO.@I"XC-_N!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">352354304</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Item Name="ModbusSRK01-05" Type="IO Server">
		<Property Name="atrb" Type="Str">@!-!!!Q1!!!8!!!!"B!!!!=!!!""!'1!:!"S!'5!=Q"T!!-!"1!!!!!!!!$Q0Q91!!!)!!!!1A"B!(5!:!"3!'%!&gt;!"F!!-!"A!!!!!!!-$#1!91!!!(!!!!2!"B!(1!91"#!'E!&gt;!!$!!5!!!!!!!!!)%!'%!!!#1!!!%E!5!""!'1!:!"S!'5!=Q"T!!91!!!-!!!!-1!S!$A!,A!T!#Y!.1!Y!#Y!-1!R!$5!"B!!!!Y!!!"*!&amp;!!11"E!'1!=A"F!(-!=Q""!'Q!;1"B!(-!"B!!!!Q!!!!R!$)!/!!O!$-!,A!V!$A!,A!R!$%!.1!'%!!!#!!!!%Q!5Q"8!%9!;1"S!(-!&gt;!!#!!!!!!91!!!(!!!!41"B!(A!1Q"P!'E!&lt;!!$!!5!!!!!!!!!-%!'%!!!#Q!!!%U!91"Y!%1!;1"T!'-!=A"F!(1!:1!$!!5!!!!!!!!!2%!'%!!!#A!!!%U!91"Y!%A!&lt;Q"M!'1!;1"O!'=!!Q!&amp;!!!!!!!!!#"!"B!!!!A!!!".!'%!?!"*!'Y!=!"V!(1!!Q!&amp;!!!!!!!!!#2!"B!!!!5!!!".!']!:!"F!'Q!"B!!!!]!!!".!']!:!"C!(5!=Q!A!%5!&gt;!"I!'5!=A"O!'5!&gt;!!'%!!!"!!!!%Y!91"N!'5!"B!!!!=!!!".!']!:!"C!(5!=Q!R!!91!!!'!!!!5!"B!()!;1"U!(E!"B!!!!1!!!"O!']!&lt;A"F!!91!!!(!!!!5!"I!']!&lt;A"F!%Y!&lt;Q!'%!!!!!!!!!91!!!)!!!!5!"P!'Q!&lt;!"3!'%!&gt;!"F!!-!!!!!!!!!!!$Q0Q91!!!)!!!!5!"S!'E!&lt;Q"S!'E!&gt;!"Z!!-!"1!!!!!!!!!A1!91!!!(!!!!5A"F!(1!=A"J!'5!=Q!$!!5!!!!!!!!!%%!'%!!!#A!!!&amp;-!:1"S!'E!91"M!&amp;!!&lt;Q"S!(1!"B!!!!1!!!"$!%]!41!R!!91!!!&amp;!!!!5Q"L!'E!=!"T!!-!"1!!!!!!!!!51!91!!!(!!!!5Q"U!']!=!"#!'E!&gt;!!'%!!!!1!!!$%!"B!!!!=!!!"5!'E!&lt;1"F!']!&gt;1"U!!-!"A!!!!!!!%"`1!91!!!*!!!!6!"S!'%!&lt;A"T!%U!&lt;Q"E!'5!"B!!!!-!!!"3!&amp;1!61!'%!!!"Q!!!&amp;5!=Q"F!%9!1Q!R!$9!!A!!!!!</Property>
		<Property Name="className" Type="Str">Modbus</Property>
	</Item>
	<Item Name="ModbusSRK06-10" Type="IO Server">
		<Property Name="atrb" Type="Str">@!-!!!Q1!!!8!!!!"B!!!!=!!!""!'1!:!"S!'5!=Q"T!!-!"1!!!!!!!!$Q0Q91!!!)!!!!1A"B!(5!:!"3!'%!&gt;!"F!!-!"A!!!!!!!-$#1!91!!!(!!!!2!"B!(1!91"#!'E!&gt;!!$!!5!!!!!!!!!)%!'%!!!#1!!!%E!5!""!'1!:!"S!'5!=Q"T!!91!!!-!!!!-1!S!$A!,A!T!#Y!.1!Y!#Y!-1!R!$9!"B!!!!Y!!!"*!&amp;!!11"E!'1!=A"F!(-!=Q""!'Q!;1"B!(-!"B!!!!Q!!!!R!$)!/!!O!$-!,A!V!$A!,A!R!$%!.A!'%!!!#!!!!%Q!5Q"8!%9!;1"S!(-!&gt;!!#!!!!!!91!!!(!!!!41"B!(A!1Q"P!'E!&lt;!!$!!5!!!!!!!!!-%!'%!!!#Q!!!%U!91"Y!%1!;1"T!'-!=A"F!(1!:1!$!!5!!!!!!!!!2%!'%!!!#A!!!%U!91"Y!%A!&lt;Q"M!'1!;1"O!'=!!Q!&amp;!!!!!!!!!#"!"B!!!!A!!!".!'%!?!"*!'Y!=!"V!(1!!Q!&amp;!!!!!!!!!#2!"B!!!!5!!!".!']!:!"F!'Q!"B!!!!]!!!".!']!:!"C!(5!=Q!A!%5!&gt;!"I!'5!=A"O!'5!&gt;!!'%!!!"!!!!%Y!91"N!'5!"B!!!!=!!!".!']!:!"C!(5!=Q!R!!91!!!'!!!!5!"B!()!;1"U!(E!"B!!!!1!!!"O!']!&lt;A"F!!91!!!(!!!!5!"I!']!&lt;A"F!%Y!&lt;Q!'%!!!!!!!!!91!!!)!!!!5!"P!'Q!&lt;!"3!'%!&gt;!"F!!-!!!!!!!!!!!$Q0Q91!!!)!!!!5!"S!'E!&lt;Q"S!'E!&gt;!"Z!!-!"1!!!!!!!!!A1!91!!!(!!!!5A"F!(1!=A"J!'5!=Q!$!!5!!!!!!!!!%%!'%!!!#A!!!&amp;-!:1"S!'E!91"M!&amp;!!&lt;Q"S!(1!"B!!!!1!!!"$!%]!41!R!!91!!!&amp;!!!!5Q"L!'E!=!"T!!-!"1!!!!!!!!!51!91!!!(!!!!5Q"U!']!=!"#!'E!&gt;!!'%!!!!1!!!$%!"B!!!!=!!!"5!'E!&lt;1"F!']!&gt;1"U!!-!"A!!!!!!!%"`1!91!!!*!!!!6!"S!'%!&lt;A"T!%U!&lt;Q"E!'5!"B!!!!-!!!"3!&amp;1!61!'%!!!"Q!!!&amp;5!=Q"F!%9!1Q!R!$9!!A!!!!!</Property>
		<Property Name="className" Type="Str">Modbus</Property>
	</Item>
	<Item Name="ModbusSRK11-15" Type="IO Server">
		<Property Name="atrb" Type="Str">CA-!!!Q1!!!8!!!!"B!!!!=!!!""!'1!:!"S!'5!=Q"T!!-!"1!!!!!!!!$Q0Q91!!!)!!!!1A"B!(5!:!"3!'%!&gt;!"F!!-!"A!!!!!!!-$#1!91!!!(!!!!2!"B!(1!91"#!'E!&gt;!!$!!5!!!!!!!!!)%!'%!!!#1!!!%E!5!""!'1!:!"S!'5!=Q"T!!91!!!-!!!!-1!S!$A!,A!T!#Y!.1!Y!#Y!-1!R!$=!"B!!!!Y!!!"*!&amp;!!11"E!'1!=A"F!(-!=Q""!'Q!;1"B!(-!"B!!!!Q!!!!R!$)!/!!O!$-!,A!V!$A!,A!R!$%!.Q!'%!!!#!!!!%Q!5Q"8!%9!;1"S!(-!&gt;!!#!!!!!!91!!!(!!!!41"B!(A!1Q"P!'E!&lt;!!$!!5!!!!!!!!!-%!'%!!!#Q!!!%U!91"Y!%1!;1"T!'-!=A"F!(1!:1!$!!5!!!!!!!!!2%!'%!!!#A!!!%U!91"Y!%A!&lt;Q"M!'1!;1"O!'=!!Q!&amp;!!!!!!!!!#"!"B!!!!A!!!".!'%!?!"*!'Y!=!"V!(1!!Q!&amp;!!!!!!!!!#2!"B!!!!5!!!".!']!:!"F!'Q!"B!!!!]!!!".!']!:!"C!(5!=Q!A!%5!&gt;!"I!'5!=A"O!'5!&gt;!!'%!!!"!!!!%Y!91"N!'5!"B!!!!Y!!!".!']!:!"C!(5!=Q"4!&amp;)!3Q!R!$%!,1!R!$5!"B!!!!9!!!"1!'%!=A"J!(1!?1!'%!!!"!!!!'Y!&lt;Q"O!'5!"B!!!!=!!!"1!'A!&lt;Q"O!'5!4A"P!!91!!!!!!!!"B!!!!A!!!"1!']!&lt;!"M!&amp;)!91"U!'5!!Q!!!!!!!!!!!0!`"B!!!!A!!!"1!()!;1"P!()!;1"U!(E!!Q!&amp;!!!!!!!!!#"!"B!!!!=!!!"3!'5!&gt;!"S!'E!:1"T!!-!"1!!!!!!!!!11!91!!!+!!!!5Q"F!()!;1"B!'Q!5!"P!()!&gt;!!'%!!!"!!!!%-!4Q".!$%!"B!!!!5!!!"4!'M!;1"Q!(-!!Q!&amp;!!!!!!!!!"2!"B!!!!=!!!"4!(1!&lt;Q"Q!%)!;1"U!!91!!!"!!!!-1!'%!!!"Q!!!&amp;1!;1"N!'5!&lt;Q"V!(1!!Q!'!!!!!!!!1(^!"B!!!!E!!!"5!()!91"O!(-!41"P!'1!:1!'%!!!!Q!!!&amp;)!6!"6!!91!!!(!!!!61"T!'5!2A"$!$%!.A!#!!!!!!</Property>
		<Property Name="className" Type="Str">Modbus</Property>
	</Item>
	<Item Name="ModbusSRK16-20" Type="IO Server">
		<Property Name="atrb" Type="Str">CA-!!!Q1!!!8!!!!"B!!!!=!!!""!'1!:!"S!'5!=Q"T!!-!"1!!!!!!!!$Q0Q91!!!)!!!!1A"B!(5!:!"3!'%!&gt;!"F!!-!"A!!!!!!!-$#1!91!!!(!!!!2!"B!(1!91"#!'E!&gt;!!$!!5!!!!!!!!!)%!'%!!!#1!!!%E!5!""!'1!:!"S!'5!=Q"T!!91!!!-!!!!-1!S!$A!,A!T!#Y!.1!Y!#Y!-1!R!$A!"B!!!!Y!!!"*!&amp;!!11"E!'1!=A"F!(-!=Q""!'Q!;1"B!(-!"B!!!!Q!!!!R!$)!/!!O!$-!,A!V!$A!,A!R!$%!/!!'%!!!#!!!!%Q!5Q"8!%9!;1"S!(-!&gt;!!#!!!!!!91!!!(!!!!41"B!(A!1Q"P!'E!&lt;!!$!!5!!!!!!!!!-%!'%!!!#Q!!!%U!91"Y!%1!;1"T!'-!=A"F!(1!:1!$!!5!!!!!!!!!2%!'%!!!#A!!!%U!91"Y!%A!&lt;Q"M!'1!;1"O!'=!!Q!&amp;!!!!!!!!!#"!"B!!!!A!!!".!'%!?!"*!'Y!=!"V!(1!!Q!&amp;!!!!!!!!!#2!"B!!!!5!!!".!']!:!"F!'Q!"B!!!!]!!!".!']!:!"C!(5!=Q!A!%5!&gt;!"I!'5!=A"O!'5!&gt;!!'%!!!"!!!!%Y!91"N!'5!"B!!!!Y!!!".!']!:!"C!(5!=Q"4!&amp;)!3Q!R!$9!,1!S!$!!"B!!!!9!!!"1!'%!=A"J!(1!?1!'%!!!"!!!!'Y!&lt;Q"O!'5!"B!!!!=!!!"1!'A!&lt;Q"O!'5!4A"P!!91!!!!!!!!"B!!!!A!!!"1!']!&lt;!"M!&amp;)!91"U!'5!!Q!!!!!!!!!!!0!`"B!!!!A!!!"1!()!;1"P!()!;1"U!(E!!Q!&amp;!!!!!!!!!#"!"B!!!!=!!!"3!'5!&gt;!"S!'E!:1"T!!-!"1!!!!!!!!!11!91!!!+!!!!5Q"F!()!;1"B!'Q!5!"P!()!&gt;!!'%!!!"!!!!%-!4Q".!$%!"B!!!!5!!!"4!'M!;1"Q!(-!!Q!&amp;!!!!!!!!!"2!"B!!!!=!!!"4!(1!&lt;Q"Q!%)!;1"U!!91!!!"!!!!-1!'%!!!"Q!!!&amp;1!;1"N!'5!&lt;Q"V!(1!!Q!'!!!!!!!!1(^!"B!!!!E!!!"5!()!91"O!(-!41"P!'1!:1!'%!!!!Q!!!&amp;)!6!"6!!91!!!(!!!!61"T!'5!2A"$!$%!.A!#!!!!!!</Property>
		<Property Name="className" Type="Str">Modbus</Property>
	</Item>
	<Item Name="ModbusSRK21-25" Type="IO Server">
		<Property Name="atrb" Type="Str">@!-!!!Q1!!!8!!!!"B!!!!=!!!""!'1!:!"S!'5!=Q"T!!-!"1!!!!!!!!$Q0Q91!!!)!!!!1A"B!(5!:!"3!'%!&gt;!"F!!-!"A!!!!!!!-$#1!91!!!(!!!!2!"B!(1!91"#!'E!&gt;!!$!!5!!!!!!!!!)%!'%!!!#1!!!%E!5!""!'1!:!"S!'5!=Q"T!!91!!!-!!!!-1!S!$A!,A!T!#Y!.1!Y!#Y!-1!R!$E!"B!!!!Y!!!"*!&amp;!!11"E!'1!=A"F!(-!=Q""!'Q!;1"B!(-!"B!!!!Q!!!!R!$)!/!!O!$-!,A!V!$A!,A!R!$%!/1!'%!!!#!!!!%Q!5Q"8!%9!;1"S!(-!&gt;!!#!!!!!!91!!!(!!!!41"B!(A!1Q"P!'E!&lt;!!$!!5!!!!!!!!!-%!'%!!!#Q!!!%U!91"Y!%1!;1"T!'-!=A"F!(1!:1!$!!5!!!!!!!!!2%!'%!!!#A!!!%U!91"Y!%A!&lt;Q"M!'1!;1"O!'=!!Q!&amp;!!!!!!!!!#"!"B!!!!A!!!".!'%!?!"*!'Y!=!"V!(1!!Q!&amp;!!!!!!!!!#2!"B!!!!5!!!".!']!:!"F!'Q!"B!!!!]!!!".!']!:!"C!(5!=Q!A!%5!&gt;!"I!'5!=A"O!'5!&gt;!!'%!!!"!!!!%Y!91"N!'5!"B!!!!=!!!".!']!:!"C!(5!=Q!R!!91!!!'!!!!5!"B!()!;1"U!(E!"B!!!!1!!!"O!']!&lt;A"F!!91!!!(!!!!5!"I!']!&lt;A"F!%Y!&lt;Q!'%!!!!!!!!!91!!!)!!!!5!"P!'Q!&lt;!"3!'%!&gt;!"F!!-!!!!!!!!!!!$Q0Q91!!!)!!!!5!"S!'E!&lt;Q"S!'E!&gt;!"Z!!-!"1!!!!!!!!!A1!91!!!(!!!!5A"F!(1!=A"J!'5!=Q!$!!5!!!!!!!!!%%!'%!!!#A!!!&amp;-!:1"S!'E!91"M!&amp;!!&lt;Q"S!(1!"B!!!!1!!!"$!%]!41!R!!91!!!&amp;!!!!5Q"L!'E!=!"T!!-!"1!!!!!!!!!51!91!!!(!!!!5Q"U!']!=!"#!'E!&gt;!!'%!!!!1!!!$%!"B!!!!=!!!"5!'E!&lt;1"F!']!&gt;1"U!!-!"A!!!!!!!%"`1!91!!!*!!!!6!"S!'%!&lt;A"T!%U!&lt;Q"E!'5!"B!!!!-!!!"3!&amp;1!61!'%!!!"Q!!!&amp;5!=Q"F!%9!1Q!R!$9!!A!!!!!</Property>
		<Property Name="className" Type="Str">Modbus</Property>
	</Item>
	<Item Name="ModbusSRK26-28" Type="IO Server">
		<Property Name="atrb" Type="Str">@!-!!!Q1!!!8!!!!"B!!!!=!!!""!'1!:!"S!'5!=Q"T!!-!"1!!!!!!!!$Q0Q91!!!)!!!!1A"B!(5!:!"3!'%!&gt;!"F!!-!"A!!!!!!!-$#1!91!!!(!!!!2!"B!(1!91"#!'E!&gt;!!$!!5!!!!!!!!!)%!'%!!!#1!!!%E!5!""!'1!:!"S!'5!=Q"T!!91!!!-!!!!-1!S!$A!,A!T!#Y!.1!Y!#Y!-1!S!$!!"B!!!!Y!!!"*!&amp;!!11"E!'1!=A"F!(-!=Q""!'Q!;1"B!(-!"B!!!!Q!!!!R!$)!/!!O!$-!,A!V!$A!,A!R!$)!-!!'%!!!#!!!!%Q!5Q"8!%9!;1"S!(-!&gt;!!#!!!!!!91!!!(!!!!41"B!(A!1Q"P!'E!&lt;!!$!!5!!!!!!!!!-%!'%!!!#Q!!!%U!91"Y!%1!;1"T!'-!=A"F!(1!:1!$!!5!!!!!!!!!2%!'%!!!#A!!!%U!91"Y!%A!&lt;Q"M!'1!;1"O!'=!!Q!&amp;!!!!!!!!!#"!"B!!!!A!!!".!'%!?!"*!'Y!=!"V!(1!!Q!&amp;!!!!!!!!!#2!"B!!!!5!!!".!']!:!"F!'Q!"B!!!!]!!!".!']!:!"C!(5!=Q!A!%5!&gt;!"I!'5!=A"O!'5!&gt;!!'%!!!"!!!!%Y!91"N!'5!"B!!!!=!!!".!']!:!"C!(5!=Q!R!!91!!!'!!!!5!"B!()!;1"U!(E!"B!!!!1!!!"O!']!&lt;A"F!!91!!!(!!!!5!"I!']!&lt;A"F!%Y!&lt;Q!'%!!!!!!!!!91!!!)!!!!5!"P!'Q!&lt;!"3!'%!&gt;!"F!!-!!!!!!!!!!!$Q0Q91!!!)!!!!5!"S!'E!&lt;Q"S!'E!&gt;!"Z!!-!"1!!!!!!!!!A1!91!!!(!!!!5A"F!(1!=A"J!'5!=Q!$!!5!!!!!!!!!%%!'%!!!#A!!!&amp;-!:1"S!'E!91"M!&amp;!!&lt;Q"S!(1!"B!!!!1!!!"$!%]!41!R!!91!!!&amp;!!!!5Q"L!'E!=!"T!!-!"1!!!!!!!!!51!91!!!(!!!!5Q"U!']!=!"#!'E!&gt;!!'%!!!!1!!!$%!"B!!!!=!!!"5!'E!&lt;1"F!']!&gt;1"U!!-!"A!!!!!!!%"`1!91!!!*!!!!6!"S!'%!&lt;A"T!%U!&lt;Q"E!'5!"B!!!!-!!!"3!&amp;1!61!'%!!!"Q!!!&amp;5!=Q"F!%9!1Q!R!$9!!A!!!!!</Property>
		<Property Name="className" Type="Str">Modbus</Property>
	</Item>
	<Item Name="SRK01-AC" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\000003</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000003</Property>
	</Item>
	<Item Name="SRK01-current" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\S300002</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!N!!A!&amp;37ZU-49!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">S300002</Property>
	</Item>
	<Item Name="SRK01-HV" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\000002</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000002</Property>
	</Item>
	<Item Name="SRK01-Status" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\A100001L7</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"#0A!!!"5!A!!!!!!#!!R!)1&gt;#&lt;W^M:7&amp;O!"Z!1!!"`````Q!!%%&amp;S=G&amp;Z)'^G)%*P&lt;WRF97Y!!!%!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">A100001L7</Property>
	</Item>
	<Item Name="SRK01-voltage" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\S300001</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!N!!A!&amp;37ZU-49!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">S300001</Property>
	</Item>
	<Item Name="SRK01-Voltage-set" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\400001</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!D(Q!!!"5!A!!!!!!"!!V!"A!'65FO&gt;$%W!!!"!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">400001</Property>
	</Item>
	<Item Name="SRK02-AC" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\000006</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000006</Property>
	</Item>
	<Item Name="SRK02-current" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\S300004</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!N!!A!&amp;37ZU-49!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">S300004</Property>
	</Item>
	<Item Name="SRK02-HV" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\000005</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000005</Property>
	</Item>
	<Item Name="SRK02-Status" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\A100009L7</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"#0A!!!"5!A!!!!!!#!!R!)1&gt;#&lt;W^M:7&amp;O!"Z!1!!"`````Q!!%%&amp;S=G&amp;Z)'^G)%*P&lt;WRF97Y!!!%!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">A100009L7</Property>
	</Item>
	<Item Name="SRK02-voltage" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\S300003</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!N!!A!&amp;37ZU-49!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">S300003</Property>
	</Item>
	<Item Name="SRK02-Voltage-set" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\400002</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!D(Q!!!"5!A!!!!!!"!!V!"A!'65FO&gt;$%W!!!"!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">400002</Property>
	</Item>
	<Item Name="SRK03-AC" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\000009</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000009</Property>
	</Item>
	<Item Name="SRK03-current" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\S300006</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!N!!A!&amp;37ZU-49!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">S300006</Property>
	</Item>
	<Item Name="SRK03-HV" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\000008</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000008</Property>
	</Item>
	<Item Name="SRK03-Status" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\A100017L7</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"#0A!!!"5!A!!!!!!#!!R!)1&gt;#&lt;W^M:7&amp;O!"Z!1!!"`````Q!!%%&amp;S=G&amp;Z)'^G)%*P&lt;WRF97Y!!!%!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">A100017L7</Property>
	</Item>
	<Item Name="SRK03-voltage" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\S300005</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!N!!A!&amp;37ZU-49!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">S300005</Property>
	</Item>
	<Item Name="SRK03-Voltage-set" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\400003</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!D(Q!!!"5!A!!!!!!"!!V!"A!'65FO&gt;$%W!!!"!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">400003</Property>
	</Item>
	<Item Name="SRK04-AC" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\000012</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000012</Property>
	</Item>
	<Item Name="SRK04-current" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\S300008</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!N!!A!&amp;37ZU-49!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">S300008</Property>
	</Item>
	<Item Name="SRK04-HV" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\000011</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000011</Property>
	</Item>
	<Item Name="SRK04-Status" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\A100025L7</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"#0A!!!"5!A!!!!!!#!!R!)1&gt;#&lt;W^M:7&amp;O!"Z!1!!"`````Q!!%%&amp;S=G&amp;Z)'^G)%*P&lt;WRF97Y!!!%!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">A100025L7</Property>
	</Item>
	<Item Name="SRK04-voltage" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\S300007</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!N!!A!&amp;37ZU-49!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">S300007</Property>
	</Item>
	<Item Name="SRK04-Voltage-set" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\400004</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!D(Q!!!"5!A!!!!!!"!!V!"A!'65FO&gt;$%W!!!"!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">400004</Property>
	</Item>
	<Item Name="SRK05-AC" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\000015</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000015</Property>
	</Item>
	<Item Name="SRK05-current" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\S300010</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!N!!A!&amp;37ZU-49!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">S300010</Property>
	</Item>
	<Item Name="SRK05-HV" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\000014</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000014</Property>
	</Item>
	<Item Name="SRK05-Status" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\A100033L7</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"#0A!!!"5!A!!!!!!#!!R!)1&gt;#&lt;W^M:7&amp;O!"Z!1!!"`````Q!!%%&amp;S=G&amp;Z)'^G)%*P&lt;WRF97Y!!!%!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">A100033L7</Property>
	</Item>
	<Item Name="SRK05-voltage" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\S300009</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!N!!A!&amp;37ZU-49!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">S300009</Property>
	</Item>
	<Item Name="SRK05-Voltage-set" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\400005</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!D(Q!!!"5!A!!!!!!"!!V!"A!'65FO&gt;$%W!!!"!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK01-05\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">400005</Property>
	</Item>
	<Item Name="SRK06-AC" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\000003</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\000003</Property>
		<Property Name="varSourceProjectSubPath" Type="Str"></Property>
	</Item>
	<Item Name="SRK06-current" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\S300002</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!N!!A!&amp;37ZU-49!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\S300002</Property>
		<Property Name="varSourceProjectSubPath" Type="Str"></Property>
	</Item>
	<Item Name="SRK06-HV" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\000002</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\000002</Property>
		<Property Name="varSourceProjectSubPath" Type="Str"></Property>
	</Item>
	<Item Name="SRK06-Status" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\A100001L7</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"#0A!!!"5!A!!!!!!#!!R!)1&gt;#&lt;W^M:7&amp;O!"Z!1!!"`````Q!!%%&amp;S=G&amp;Z)'^G)%*P&lt;WRF97Y!!!%!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\A100001L7</Property>
		<Property Name="varSourceProjectSubPath" Type="Str"></Property>
	</Item>
	<Item Name="SRK06-voltage" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\S300001</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!N!!A!&amp;37ZU-49!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\S300001</Property>
		<Property Name="varSourceProjectSubPath" Type="Str"></Property>
	</Item>
	<Item Name="SRK06-Voltage-set" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\400001</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!D(Q!!!"5!A!!!!!!"!!V!"A!'65FO&gt;$%W!!!"!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\400001</Property>
		<Property Name="varSourceProjectSubPath" Type="Str"></Property>
	</Item>
	<Item Name="SRK07-AC" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\000006</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\000006</Property>
		<Property Name="varSourceProjectSubPath" Type="Str"></Property>
	</Item>
	<Item Name="SRK07-current" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\S300004</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!N!!A!&amp;37ZU-49!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\S300004</Property>
		<Property Name="varSourceProjectSubPath" Type="Str"></Property>
	</Item>
	<Item Name="SRK07-HV" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\000005</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\000005</Property>
		<Property Name="varSourceProjectSubPath" Type="Str"></Property>
	</Item>
	<Item Name="SRK07-Status" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\A100009L7</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"#0A!!!"5!A!!!!!!#!!R!)1&gt;#&lt;W^M:7&amp;O!"Z!1!!"`````Q!!%%&amp;S=G&amp;Z)'^G)%*P&lt;WRF97Y!!!%!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\A100009L7</Property>
		<Property Name="varSourceProjectSubPath" Type="Str"></Property>
	</Item>
	<Item Name="SRK07-voltage" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\S300003</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!N!!A!&amp;37ZU-49!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\S300003</Property>
		<Property Name="varSourceProjectSubPath" Type="Str"></Property>
	</Item>
	<Item Name="SRK07-Voltage-set" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\400002</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!D(Q!!!"5!A!!!!!!"!!V!"A!'65FO&gt;$%W!!!"!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\400002</Property>
		<Property Name="varSourceProjectSubPath" Type="Str"></Property>
	</Item>
	<Item Name="SRK08-AC" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\000009</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\000009</Property>
		<Property Name="varSourceProjectSubPath" Type="Str"></Property>
	</Item>
	<Item Name="SRK08-current" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\S300006</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!N!!A!&amp;37ZU-49!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\S300006</Property>
		<Property Name="varSourceProjectSubPath" Type="Str"></Property>
	</Item>
	<Item Name="SRK08-HV" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\000008</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\000008</Property>
		<Property Name="varSourceProjectSubPath" Type="Str"></Property>
	</Item>
	<Item Name="SRK08-Status" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\A100017L7</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"#0A!!!"5!A!!!!!!#!!R!)1&gt;#&lt;W^M:7&amp;O!"Z!1!!"`````Q!!%%&amp;S=G&amp;Z)'^G)%*P&lt;WRF97Y!!!%!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\A100017L7</Property>
		<Property Name="varSourceProjectSubPath" Type="Str"></Property>
	</Item>
	<Item Name="SRK08-voltage" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\S300005</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!N!!A!&amp;37ZU-49!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\S300005</Property>
		<Property Name="varSourceProjectSubPath" Type="Str"></Property>
	</Item>
	<Item Name="SRK08-Voltage-set" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\400003</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!D(Q!!!"5!A!!!!!!"!!V!"A!'65FO&gt;$%W!!!"!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\400003</Property>
		<Property Name="varSourceProjectSubPath" Type="Str"></Property>
	</Item>
	<Item Name="SRK09-AC" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\000012</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\000012</Property>
		<Property Name="varSourceProjectSubPath" Type="Str"></Property>
	</Item>
	<Item Name="SRK09-current" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\S300008</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!N!!A!&amp;37ZU-49!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\S300008</Property>
		<Property Name="varSourceProjectSubPath" Type="Str"></Property>
	</Item>
	<Item Name="SRK09-HV" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\000011</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\000011</Property>
		<Property Name="varSourceProjectSubPath" Type="Str"></Property>
	</Item>
	<Item Name="SRK09-Status" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\A100025L7</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"#0A!!!"5!A!!!!!!#!!R!)1&gt;#&lt;W^M:7&amp;O!"Z!1!!"`````Q!!%%&amp;S=G&amp;Z)'^G)%*P&lt;WRF97Y!!!%!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\A100025L7</Property>
		<Property Name="varSourceProjectSubPath" Type="Str"></Property>
	</Item>
	<Item Name="SRK09-voltage" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\S300007</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!N!!A!&amp;37ZU-49!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\S300007</Property>
		<Property Name="varSourceProjectSubPath" Type="Str"></Property>
	</Item>
	<Item Name="SRK09-Voltage-set" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\400004</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!D(Q!!!"5!A!!!!!!"!!V!"A!'65FO&gt;$%W!!!"!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\400004</Property>
		<Property Name="varSourceProjectSubPath" Type="Str"></Property>
	</Item>
	<Item Name="SRK10-AC" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\000015</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\000015</Property>
		<Property Name="varSourceProjectSubPath" Type="Str"></Property>
	</Item>
	<Item Name="SRK10-current" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\S300010</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!N!!A!&amp;37ZU-49!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\S300010</Property>
		<Property Name="varSourceProjectSubPath" Type="Str"></Property>
	</Item>
	<Item Name="SRK10-HV" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\000014</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\000014</Property>
		<Property Name="varSourceProjectSubPath" Type="Str"></Property>
	</Item>
	<Item Name="SRK10-Status" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\A100033L7</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"#0A!!!"5!A!!!!!!#!!R!)1&gt;#&lt;W^M:7&amp;O!"Z!1!!"`````Q!!%%&amp;S=G&amp;Z)'^G)%*P&lt;WRF97Y!!!%!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\A100033L7</Property>
		<Property Name="varSourceProjectSubPath" Type="Str"></Property>
	</Item>
	<Item Name="SRK10-voltage" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\S300009</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!N!!A!&amp;37ZU-49!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\S300009</Property>
		<Property Name="varSourceProjectSubPath" Type="Str"></Property>
	</Item>
	<Item Name="SRK10-Voltage-set" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\400005</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!D(Q!!!"5!A!!!!!!"!!V!"A!'65FO&gt;$%W!!!"!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK06-10\400005</Property>
		<Property Name="varSourceProjectSubPath" Type="Str"></Property>
	</Item>
	<Item Name="SRK11-AC" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\000003</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000003</Property>
	</Item>
	<Item Name="SRK11-current" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\S300002</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!N!!A!&amp;37ZU-49!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">S300002</Property>
	</Item>
	<Item Name="SRK11-HV" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\000002</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000002</Property>
	</Item>
	<Item Name="SRK11-Status" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\A100001L7</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"#0A!!!"5!A!!!!!!#!!R!)1&gt;#&lt;W^M:7&amp;O!"Z!1!!"`````Q!!%%&amp;S=G&amp;Z)'^G)%*P&lt;WRF97Y!!!%!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">A100001L7</Property>
	</Item>
	<Item Name="SRK11-voltage" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\S300001</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!N!!A!&amp;37ZU-49!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">S300001</Property>
	</Item>
	<Item Name="SRK11-Voltage-set" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\400001</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!D(Q!!!"5!A!!!!!!"!!V!"A!'65FO&gt;$%W!!!"!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">400001</Property>
	</Item>
	<Item Name="SRK12-AC" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\000006</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000006</Property>
	</Item>
	<Item Name="SRK12-current" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\S300004</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!N!!A!&amp;37ZU-49!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">S300004</Property>
	</Item>
	<Item Name="SRK12-HV" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\000005</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000005</Property>
	</Item>
	<Item Name="SRK12-Status" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\A100009L7</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"#0A!!!"5!A!!!!!!#!!R!)1&gt;#&lt;W^M:7&amp;O!"Z!1!!"`````Q!!%%&amp;S=G&amp;Z)'^G)%*P&lt;WRF97Y!!!%!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">A100009L7</Property>
	</Item>
	<Item Name="SRK12-voltage" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\S300003</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!N!!A!&amp;37ZU-49!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">S300003</Property>
	</Item>
	<Item Name="SRK12-Voltage-set" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\400002</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!D(Q!!!"5!A!!!!!!"!!V!"A!'65FO&gt;$%W!!!"!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">400002</Property>
	</Item>
	<Item Name="SRK13-AC" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\000009</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000009</Property>
	</Item>
	<Item Name="SRK13-current" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\S300006</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!N!!A!&amp;37ZU-49!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">S300006</Property>
	</Item>
	<Item Name="SRK13-HV" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\000008</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000008</Property>
	</Item>
	<Item Name="SRK13-Status" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\A100017L7</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"#0A!!!"5!A!!!!!!#!!R!)1&gt;#&lt;W^M:7&amp;O!"Z!1!!"`````Q!!%%&amp;S=G&amp;Z)'^G)%*P&lt;WRF97Y!!!%!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">A100017L7</Property>
	</Item>
	<Item Name="SRK13-voltage" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\S300005</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!N!!A!&amp;37ZU-49!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">S300005</Property>
	</Item>
	<Item Name="SRK13-Voltage-set" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\400003</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!D(Q!!!"5!A!!!!!!"!!V!"A!'65FO&gt;$%W!!!"!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">400003</Property>
	</Item>
	<Item Name="SRK14-AC" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\000012</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000012</Property>
	</Item>
	<Item Name="SRK14-current" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\S300008</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!N!!A!&amp;37ZU-49!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">S300008</Property>
	</Item>
	<Item Name="SRK14-HV" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\000011</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000011</Property>
	</Item>
	<Item Name="SRK14-Status" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\A100025L7</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"#0A!!!"5!A!!!!!!#!!R!)1&gt;#&lt;W^M:7&amp;O!"Z!1!!"`````Q!!%%&amp;S=G&amp;Z)'^G)%*P&lt;WRF97Y!!!%!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">A100025L7</Property>
	</Item>
	<Item Name="SRK14-voltage" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\S300007</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!N!!A!&amp;37ZU-49!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">S300007</Property>
	</Item>
	<Item Name="SRK14-Voltage-set" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\400004</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!D(Q!!!"5!A!!!!!!"!!V!"A!'65FO&gt;$%W!!!"!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">400004</Property>
	</Item>
	<Item Name="SRK15-AC" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\000015</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000015</Property>
	</Item>
	<Item Name="SRK15-current" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\S300010</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!N!!A!&amp;37ZU-49!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">S300010</Property>
	</Item>
	<Item Name="SRK15-HV" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\000014</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000014</Property>
	</Item>
	<Item Name="SRK15-Status" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\A100033L7</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"#0A!!!"5!A!!!!!!#!!R!)1&gt;#&lt;W^M:7&amp;O!"Z!1!!"`````Q!!%%&amp;S=G&amp;Z)'^G)%*P&lt;WRF97Y!!!%!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">A100033L7</Property>
	</Item>
	<Item Name="SRK15-voltage" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\S300009</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!N!!A!&amp;37ZU-49!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">S300009</Property>
	</Item>
	<Item Name="SRK15-Voltage-set" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\400005</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!D(Q!!!"5!A!!!!!!"!!V!"A!'65FO&gt;$%W!!!"!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK11-15\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">400005</Property>
	</Item>
	<Item Name="SRK16-AC" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\000003</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000003</Property>
	</Item>
	<Item Name="SRK16-current" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\S300002</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!N!!A!&amp;37ZU-49!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">S300002</Property>
	</Item>
	<Item Name="SRK16-HV" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\000002</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000002</Property>
	</Item>
	<Item Name="SRK16-Status" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\A100001L7</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"#0A!!!"5!A!!!!!!#!!R!)1&gt;#&lt;W^M:7&amp;O!"Z!1!!"`````Q!!%%&amp;S=G&amp;Z)'^G)%*P&lt;WRF97Y!!!%!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">A100001L7</Property>
	</Item>
	<Item Name="SRK16-voltage" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\S300001</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!N!!A!&amp;37ZU-49!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">S300001</Property>
	</Item>
	<Item Name="SRK16-Voltage-set" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\400001</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!D(Q!!!"5!A!!!!!!"!!V!"A!'65FO&gt;$%W!!!"!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">400001</Property>
	</Item>
	<Item Name="SRK17-AC" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\000006</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000006</Property>
	</Item>
	<Item Name="SRK17-current" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\S300004</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!N!!A!&amp;37ZU-49!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">S300004</Property>
	</Item>
	<Item Name="SRK17-HV" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\000005</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000005</Property>
	</Item>
	<Item Name="SRK17-Status" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\A100009L7</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"#0A!!!"5!A!!!!!!#!!R!)1&gt;#&lt;W^M:7&amp;O!"Z!1!!"`````Q!!%%&amp;S=G&amp;Z)'^G)%*P&lt;WRF97Y!!!%!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">A100009L7</Property>
	</Item>
	<Item Name="SRK17-voltage" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\S300003</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!N!!A!&amp;37ZU-49!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">S300003</Property>
	</Item>
	<Item Name="SRK17-Voltage-set" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\400002</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!D(Q!!!"5!A!!!!!!"!!V!"A!'65FO&gt;$%W!!!"!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">400002</Property>
	</Item>
	<Item Name="SRK18-AC" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\000009</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000009</Property>
	</Item>
	<Item Name="SRK18-current" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\S300006</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!N!!A!&amp;37ZU-49!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">S300006</Property>
	</Item>
	<Item Name="SRK18-HV" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\000008</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000008</Property>
	</Item>
	<Item Name="SRK18-Status" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\A100017L7</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"#0A!!!"5!A!!!!!!#!!R!)1&gt;#&lt;W^M:7&amp;O!"Z!1!!"`````Q!!%%&amp;S=G&amp;Z)'^G)%*P&lt;WRF97Y!!!%!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">A100017L7</Property>
	</Item>
	<Item Name="SRK18-voltage" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\S300005</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!N!!A!&amp;37ZU-49!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">S300005</Property>
	</Item>
	<Item Name="SRK18-Voltage-set" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\400003</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!D(Q!!!"5!A!!!!!!"!!V!"A!'65FO&gt;$%W!!!"!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">400003</Property>
	</Item>
	<Item Name="SRK19-AC" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\000012</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000012</Property>
	</Item>
	<Item Name="SRK19-current" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\S300008</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!N!!A!&amp;37ZU-49!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">S300008</Property>
	</Item>
	<Item Name="SRK19-HV" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\000011</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000011</Property>
	</Item>
	<Item Name="SRK19-Status" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\A100025L7</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"#0A!!!"5!A!!!!!!#!!R!)1&gt;#&lt;W^M:7&amp;O!"Z!1!!"`````Q!!%%&amp;S=G&amp;Z)'^G)%*P&lt;WRF97Y!!!%!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">A100025L7</Property>
	</Item>
	<Item Name="SRK19-voltage" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\S300007</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!N!!A!&amp;37ZU-49!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">S300007</Property>
	</Item>
	<Item Name="SRK19-Voltage-set" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\400004</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!D(Q!!!"5!A!!!!!!"!!V!"A!'65FO&gt;$%W!!!"!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">400004</Property>
	</Item>
	<Item Name="SRK20-AC" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\000015</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000015</Property>
	</Item>
	<Item Name="SRK20-current" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\S300010</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!N!!A!&amp;37ZU-49!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">S300010</Property>
	</Item>
	<Item Name="SRK20-HV" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\000014</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000014</Property>
	</Item>
	<Item Name="SRK20-Status" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\A100033L7</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"#0A!!!"5!A!!!!!!#!!R!)1&gt;#&lt;W^M:7&amp;O!"Z!1!!"`````Q!!%%&amp;S=G&amp;Z)'^G)%*P&lt;WRF97Y!!!%!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">A100033L7</Property>
	</Item>
	<Item Name="SRK20-voltage" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\S300009</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!N!!A!&amp;37ZU-49!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">S300009</Property>
	</Item>
	<Item Name="SRK20-Voltage-set" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\400005</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!D(Q!!!"5!A!!!!!!"!!V!"A!'65FO&gt;$%W!!!"!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK16-20\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">400005</Property>
	</Item>
	<Item Name="SRK21-AC" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\000003</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000003</Property>
	</Item>
	<Item Name="SRK21-current" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\S300002</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!N!!A!&amp;37ZU-49!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">S300002</Property>
	</Item>
	<Item Name="SRK21-HV" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\000002</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000002</Property>
	</Item>
	<Item Name="SRK21-Status" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\A100001L7</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"#0A!!!"5!A!!!!!!#!!R!)1&gt;#&lt;W^M:7&amp;O!"Z!1!!"`````Q!!%%&amp;S=G&amp;Z)'^G)%*P&lt;WRF97Y!!!%!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">A100001L7</Property>
	</Item>
	<Item Name="SRK21-voltage" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\S300001</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!N!!A!&amp;37ZU-49!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">S300001</Property>
	</Item>
	<Item Name="SRK21-Voltage-set" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\400001</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!D(Q!!!"5!A!!!!!!"!!V!"A!'65FO&gt;$%W!!!"!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">400001</Property>
	</Item>
	<Item Name="SRK22-AC" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\000006</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000006</Property>
	</Item>
	<Item Name="SRK22-current" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\S300004</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!N!!A!&amp;37ZU-49!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">S300004</Property>
	</Item>
	<Item Name="SRK22-HV" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\000005</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000005</Property>
	</Item>
	<Item Name="SRK22-Status" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\A100009L7</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"#0A!!!"5!A!!!!!!#!!R!)1&gt;#&lt;W^M:7&amp;O!"Z!1!!"`````Q!!%%&amp;S=G&amp;Z)'^G)%*P&lt;WRF97Y!!!%!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">A100009L7</Property>
	</Item>
	<Item Name="SRK22-voltage" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\S300003</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!N!!A!&amp;37ZU-49!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">S300003</Property>
	</Item>
	<Item Name="SRK22-Voltage-set" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\400002</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!D(Q!!!"5!A!!!!!!"!!V!"A!'65FO&gt;$%W!!!"!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">400002</Property>
	</Item>
	<Item Name="SRK23-AC" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\000009</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000009</Property>
	</Item>
	<Item Name="SRK23-current" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\S300006</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!N!!A!&amp;37ZU-49!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">S300006</Property>
	</Item>
	<Item Name="SRK23-HV" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\000008</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000008</Property>
	</Item>
	<Item Name="SRK23-Status" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\A100017L7</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"#0A!!!"5!A!!!!!!#!!R!)1&gt;#&lt;W^M:7&amp;O!"Z!1!!"`````Q!!%%&amp;S=G&amp;Z)'^G)%*P&lt;WRF97Y!!!%!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">A100017L7</Property>
	</Item>
	<Item Name="SRK23-voltage" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\S300005</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!N!!A!&amp;37ZU-49!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">S300005</Property>
	</Item>
	<Item Name="SRK23-Voltage-set" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\400003</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!D(Q!!!"5!A!!!!!!"!!V!"A!'65FO&gt;$%W!!!"!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">400003</Property>
	</Item>
	<Item Name="SRK24-AC" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\000012</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000012</Property>
	</Item>
	<Item Name="SRK24-current" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\S300008</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!N!!A!&amp;37ZU-49!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">S300008</Property>
	</Item>
	<Item Name="SRK24-HV" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\000011</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000011</Property>
	</Item>
	<Item Name="SRK24-Status" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\A100025L7</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"#0A!!!"5!A!!!!!!#!!R!)1&gt;#&lt;W^M:7&amp;O!"Z!1!!"`````Q!!%%&amp;S=G&amp;Z)'^G)%*P&lt;WRF97Y!!!%!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">A100025L7</Property>
	</Item>
	<Item Name="SRK24-voltage" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\S300007</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!N!!A!&amp;37ZU-49!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">S300007</Property>
	</Item>
	<Item Name="SRK24-Voltage-set" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\400004</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!D(Q!!!"5!A!!!!!!"!!V!"A!'65FO&gt;$%W!!!"!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">400004</Property>
	</Item>
	<Item Name="SRK25-AC" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\000015</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000015</Property>
	</Item>
	<Item Name="SRK25-current" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\S300010</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!N!!A!&amp;37ZU-49!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">S300010</Property>
	</Item>
	<Item Name="SRK25-HV" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\000014</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000014</Property>
	</Item>
	<Item Name="SRK25-Status" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\A100033L7</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"#0A!!!"5!A!!!!!!#!!R!)1&gt;#&lt;W^M:7&amp;O!"Z!1!!"`````Q!!%%&amp;S=G&amp;Z)'^G)%*P&lt;WRF97Y!!!%!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">A100033L7</Property>
	</Item>
	<Item Name="SRK25-voltage" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\S300009</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!N!!A!&amp;37ZU-49!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">S300009</Property>
	</Item>
	<Item Name="SRK25-Voltage-set" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\400005</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!D(Q!!!"5!A!!!!!!"!!V!"A!'65FO&gt;$%W!!!"!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK21-25\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">400005</Property>
	</Item>
	<Item Name="SRK26-AC" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK26-28\000003</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK26-28\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000003</Property>
	</Item>
	<Item Name="SRK26-current" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK26-28\S300002</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!N!!A!&amp;37ZU-49!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK26-28\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">S300002</Property>
	</Item>
	<Item Name="SRK26-HV" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK26-28\000002</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK26-28\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000002</Property>
	</Item>
	<Item Name="SRK26-Status" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK26-28\A100017L7</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"#0A!!!"5!A!!!!!!#!!R!)1&gt;#&lt;W^M:7&amp;O!"Z!1!!"`````Q!!%%&amp;S=G&amp;Z)'^G)%*P&lt;WRF97Y!!!%!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK26-28\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">A100017L7</Property>
	</Item>
	<Item Name="SRK26-voltage" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK26-28\S300001</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/NDCXII controls.lvproj/My Computer/NDCXII controls main/ModbusVariables.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!N!!A!&amp;37ZU-49!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK26-28\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">S300005</Property>
	</Item>
	<Item Name="SRK26-Voltage-set" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK26-28\400001</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/NDCXII controls.lvproj/My Computer/NDCXII controls main/ModbusVariables.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!D(Q!!!"5!A!!!!!!"!!V!"A!'65FO&gt;$%W!!!"!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK26-28\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">400003</Property>
	</Item>
	<Item Name="SRK27-AC" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK26-28\000006</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK26-28\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000006</Property>
	</Item>
	<Item Name="SRK27-current" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK26-28\S300004</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!N!!A!&amp;37ZU-49!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK26-28\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">S300004</Property>
	</Item>
	<Item Name="SRK27-HV" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK26-28\000005</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK26-28\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000005</Property>
	</Item>
	<Item Name="SRK27-Status" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK26-28\A100001L7</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"#0A!!!"5!A!!!!!!#!!R!)1&gt;#&lt;W^M:7&amp;O!"Z!1!!"`````Q!!%%&amp;S=G&amp;Z)'^G)%*P&lt;WRF97Y!!!%!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK26-28\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">A100001L7</Property>
	</Item>
	<Item Name="SRK27-voltage" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK26-28\S300003</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/NDCXII controls.lvproj/My Computer/NDCXII controls main/ModbusVariables.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!N!!A!&amp;37ZU-49!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK26-28\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">S300001</Property>
	</Item>
	<Item Name="SRK27-Voltage-set" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK26-28\400002</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/NDCXII controls.lvproj/My Computer/NDCXII controls main/ModbusVariables.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!D(Q!!!"5!A!!!!!!"!!V!"A!'65FO&gt;$%W!!!"!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK26-28\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">400001</Property>
	</Item>
	<Item Name="SRK28-AC" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK26-28\000009</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK26-28\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000009</Property>
	</Item>
	<Item Name="SRK28-current" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK26-28\S300006</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!N!!A!&amp;37ZU-49!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK26-28\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">S300006</Property>
	</Item>
	<Item Name="SRK28-HV" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK26-28\000008</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK26-28\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">000008</Property>
	</Item>
	<Item Name="SRK28-Status" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK26-28\A100009L7</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"#0A!!!"5!A!!!!!!#!!R!)1&gt;#&lt;W^M:7&amp;O!"Z!1!!"`````Q!!%%&amp;S=G&amp;Z)'^G)%*P&lt;WRF97Y!!!%!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK26-28\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">A100009L7</Property>
	</Item>
	<Item Name="SRK28-voltage" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK26-28\S300005</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/NDCXII controls.lvproj/My Computer/NDCXII controls main/ModbusVariables.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!N!!A!&amp;37ZU-49!!1!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK26-28\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">S300003</Property>
	</Item>
	<Item Name="SRK28-Voltage-set" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ElemSize" Type="Str">1</Property>
		<Property Name="Network:PointsPerWaveform" Type="Str">1</Property>
		<Property Name="Network:ProjectBinding" Type="Str">True</Property>
		<Property Name="Network:ProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK26-28\400003</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">True</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="Path" Type="Str">/NDCXII controls.lvproj/My Computer/NDCXII controls main/ModbusVariables.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!D(Q!!!"5!A!!!!!!"!!V!"A!'65FO&gt;$%W!!!"!!!!!!!!!!!!!!!!</Property>
		<Property Name="varSourceProjectPath" Type="Str">My Computer\ModbusVariables.lvlib\ModbusSRK26-28\</Property>
		<Property Name="varSourceProjectSubPath" Type="Str">400002</Property>
	</Item>
</Library>
