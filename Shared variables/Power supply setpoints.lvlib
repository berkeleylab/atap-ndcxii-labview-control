﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="15008000">
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91M&lt;/W-,&lt;'&amp;&lt;9+K1,7Q,&lt;)%N&lt;!NMA3X)DW?-RJ(JQ"I\%%Z,(@`BA#==ZB3RN;]28_,V7@P_W`:R`&gt;HV*SU_WE@\N_XF[3:^^TX\+2YP)D7K6;G-RV3P)R`ZS%=_]J'XP/5N&lt;XH,7V\SEJ?]Z#5P?=J4HP+5JTTFWS%0?=B$DD1G(R/.1==!IT.+D)`B':\B'2Z@9XC':XC':XBUC?%:HO%:HO&amp;R7QT0]!T0]!S0I4&lt;*&lt;)?=:XA-(]X40-X40-VDSGC?"GC4N9(&lt;)"D2,L;4ZGG?ZH%;T&gt;-]T&gt;-]T?.S.%`T.%`T.)^&lt;NF8J4@-YZ$S'C?)JHO)JHO)R&gt;"20]220]230[;*YCK=ASI2F=)1I.Z5/Z5PR&amp;)^@54T&amp;5TT&amp;5TQO&lt;5_INJ6Z;"[(H#&gt;ZEC&gt;ZEC&gt;Z$"(*ETT*ETT*9^B)HO2*HO2*(F.&amp;]C20]C2)GN4UE1:,.[:/+5A?0^NOS?UJ^3&lt;*\9B9GT@7JISVW7*NIFC&lt;)^:$D`5Q9TWE7)M@;V&amp;D,6;M29DVR]6#R],%GC47T9_/=@&gt;Z5V&gt;V57&gt;V5E&gt;V5(OV?^T[FTP?\`?YX7ZRP6\D=LH%_8S/U_E5R_-R$I&gt;$\0@\W/VW&lt;[_"&lt;Y[X&amp;],0^^+,]T_J&gt;`J@_B_]'_.T`$KO.@I"O[^NF!!!!!!</Property>
	<Property Name="NI.Lib.LocalName" Type="Str">Shared variable 1</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">352354304</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Item Name="Injector and cell 1-7 HVPS setpoints" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:SingleWriter" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!+`OQ)!!"5!A!!!!!!3!"N!#A!537ZK:7.U&lt;X)A3&amp;9A=W6U='^J&lt;H1!!"V!#A!837ZK:7.U&lt;X)A=G6T:81A=W6U='^J&lt;H1!(U!+!"B*&lt;GJF9X2P=C"B9W.F&lt;#"T:82Q&lt;WFO&gt;#!!!"F!#A!41W6M&lt;#!D.S")6C"T:82Q&lt;WFO&gt;!!:1!I!%U.F&lt;'QA)T9A3&amp;9A=W6U='^J&lt;H1!'5!+!".$:7RM)#-V)%B7)(.F&gt;("P;7ZU!"F!#A!41W6M&lt;#!D.#")6C"T:82Q&lt;WFO&gt;!!&lt;1!I!&amp;%.F&lt;'QA)T-A3&amp;9A=W6U='^J&lt;H1A!!!&lt;1!I!&amp;%.F&lt;'QA)T)A3&amp;9A=W6U='^J&lt;H1A!!!:1!I!%U.F&lt;'QA)T%A3&amp;9A=W6U='^J&lt;H1!(5!+!":$:7RM)#-X)#"35V1A=W6U='^J&lt;H1A!!!&gt;1!I!&amp;E.F&lt;'QA)T9A)&amp;*46#"T:82Q&lt;WFO&gt;#!!!"N!#A!61W6M&lt;#!D.3!A5F.5)(.F&gt;("P;7ZU!"N!#A!51W6M&lt;#!D.#"35V1A=W6U='^J&lt;H1!!"N!#A!51W6M&lt;#!D-S"35V1A=W6U='^J&lt;H1!!"N!#A!61W6M&lt;#!D-C"35V1A=W6U='^J&lt;H1A!"N!#A!51W6M&lt;#!D-3"35V1A=W6U='^J&lt;H1!!&amp;B!5!!2!!!!!1!#!!-!"!!&amp;!!9!"Q!)!!E!#A!,!!Q!$1!/!!]!%#^*&lt;GJF9X2P=C"B&lt;G1A1W6M&lt;#!R,4=A3&amp;9A='^X:8)A=X6Q='RZ)(.F&gt;("P;7ZU=Q!"!"%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
</Library>
