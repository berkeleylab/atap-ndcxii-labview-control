﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="15008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;8"&lt;2MR%!813:!!O;K$1#V-#WJ",5Q,OPKI&amp;K9&amp;N;!7JA7VI";=JQVBZ"4F%#-ZG/O26X_ZZ$/87%&gt;M\6P%FXB^VL\_NHV=@X&lt;^39O0^N(_&lt;8NZOEH@@=^_CM?,3)VK63LD-&gt;8LS%=_]J'0@/1N&lt;XH,7^\SFJ?]Z#5P?=F,HP+5JTTF+5`Z&gt;MB$(P+1)YX*RU2DU$(![)Q3YW.YBG&gt;YBM@8'*\B':\B'2Z&gt;9HC':XC':XD=&amp;M-T0--T0-.DK%USWS(H'2\$2`-U4`-U4`/9-JKH!&gt;JE&lt;?!W#%;UC_WE?:KH?:R']T20]T20]\A=T&gt;-]T&gt;-]T?/7&lt;66[UTQ//9^BIHC+JXC+JXA-(=640-640-6DOCC?YCG)-G%:(#(+4;6$_6)]R?.8&amp;%`R&amp;%`R&amp;)^,WR/K&lt;75?GM=BZUG?Z%G?Z%E?1U4S*%`S*%`S'$;3*XG3*XG3RV320-G40!G3*D6^J-(3D;F4#J,(T\:&lt;=HN+P5FS/S,7ZIWV+7.NNFC&lt;+.&lt;GC0819TX-7!]JVO,(7N29CR6L%7,^=&lt;(1M4#R*IFV][.DX(X?V&amp;6&gt;V&amp;G&gt;V&amp;%&gt;V&amp;\N(L@_Z9\X_TVONVN=L^?Y8#ZR0J`D&gt;$L&amp;]8C-Q_%1_`U_&gt;LP&gt;WWPAO_0NB@$TP@4C`%`KH@[8`A@PRPA=PYZLD8Y![_ML^!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">352354304</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Item Name="Injector vacuum status" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:SingleWriter" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*?7A)!!"5!A!!!!!!:!""!)1N(6D%A1WRP=W6E)!!/1#%*2V9R)%^Q:7YA!"J!)22*&lt;GJF9X2P=C"Q&gt;7VQ:71A:'^X&lt;A!!(%!B&amp;UFO;G6D&gt;'^S)(:B9X6V&lt;3"U=GFQ='6E!":!)2"*&lt;GJF9X2P=C"S&lt;X6H;'6E!!!51#%06(6S9G]A&gt;G6O&gt;#"P='6O!":!)2&amp;5&gt;8*C&lt;S"*&lt;H2F=GRP9WNF:!!31#%.6(6S9G]A&gt;'FN:7^V&gt;!!11#%,6(6S9G]A:G&amp;V&lt;(1!&amp;%!B$F2V=G*P)'&amp;U)(.Q:76E!!!11#%+2V9Z)%.M&lt;X.F:!!!$E!B#%&gt;7/3"0='6O!!!/1#%*2V9R-3"0='6O!""!)1N(6D%R)%.M&lt;X.F:!!:1!I!%EFO;C"U&gt;8*C&lt;S"T='6F:#")?A!!(E!B'5FO;G6D&gt;'^S)':P=G6M;7ZF)(*P&gt;7&gt;I:71!'%!B%UF$)T%A&gt;G&amp;D&gt;86N)(2S;8"Q:71!%E!B$5F$)T%A=G^V:WBF:#!!&amp;E!B%5F$)T%A=(6N='6E)'2P&gt;WYA!""!)1N(6D)A4X"F&lt;G6E)!!31#%-2V9S)%.M&lt;X.F:#!A!!!11#%,5F9T)%.M&lt;X.F:#!!%%!B#F*7-S"0='6O:71!!"F!#A!435-D-3"U&gt;8*C&lt;S"T='6F:#")?A"-1&amp;!!'!!!!!%!!A!$!!1!"1!'!!=!#!!*!!I!#Q!-!!U!$A!0!"!!%1!3!"-!&amp;!!6!"9!&amp;R6797.V&gt;7UA:WRP9G&amp;M)'.M&gt;8.U:8)!!1!9!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
</Library>
